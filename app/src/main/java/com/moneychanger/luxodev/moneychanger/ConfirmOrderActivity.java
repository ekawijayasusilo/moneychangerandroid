package com.moneychanger.luxodev.moneychanger;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.moneychanger.luxodev.moneychanger.pojo.request.CreateOrderData;
import com.moneychanger.luxodev.moneychanger.pojo.response.CreatedOrder;
import com.moneychanger.luxodev.moneychanger.pojo.response.DetailCurrencyList;


import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfirmOrderActivity extends AppCompatActivity {

    List<DetailCurrencyList.DetailCurrency> detailCurrencyList;
    List<Integer> orderAmountList;
    List<Integer> indexListToDelete;
    List<InfoKeyValue> infoKeyValueList;
    Float orderTotal;
    Integer orderDelivery;
    //String codeCurrency;
    RecyclerView rvOrderConfirmation;
    RecyclerView rvDetailOrderConfirmation;
    ConfirmOrderAdapter customOrderAdapter;
    ConfirmDetailOrderAdapter customDetailOrderAdapter;

    SharedPreferences sharedPref;
    APIInterface apiInterface;
    AlertDialog.Builder alertDialogBuilder;
    Locale localeID;
    NumberFormat numberFormat;
    Gson gson;
    ProgressBar spinner;
    TextView Ongkir,payment;
    CardView OngkirCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.mcPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        Ongkir=(TextView) findViewById(R.id.tvOngkirTotal);
        OngkirCard=(CardView) findViewById(R.id.cardOngkirConfrimation);
        localeID = new Locale("in", "ID");
        numberFormat = NumberFormat.getCurrencyInstance(localeID);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        sharedPref = getSharedPreferences(ConfirmOrderActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
        alertDialogBuilder=new AlertDialog.Builder(ConfirmOrderActivity.this);
        gson = new Gson();

        orderAmountList=new ArrayList<>();
        orderDelivery=sharedPref.getInt("order_delivery",-1); //AMAN???
        orderTotal=sharedPref.getFloat("order_total", 0f);
        Type type = new TypeToken<List<DetailCurrencyList.DetailCurrency>>(){}.getType();
        detailCurrencyList= gson.fromJson(sharedPref.getString("detail_currency_list",null), type);
        StringTokenizer st = new StringTokenizer(sharedPref.getString("order_amount_list", ""), ",");
        while (st.hasMoreElements()){
            orderAmountList.add(Integer.parseInt(st.nextToken()));
        }
        //codeCurrency=sharedPref.getString("current_order_code_currency",null);
        payment =(TextView) findViewById(R.id.tvPayment);
        TextView tvConfirmOrderTotal=findViewById(R.id.tvConfirmOrderTotal);
        spinner = findViewById(R.id.progressBarConfirmOrder);
        spinner.setVisibility(View.GONE);

        indexListToDelete=new ArrayList<>();
        for(int i=0;i<orderAmountList.size();i++) if(orderAmountList.get(i)==0) indexListToDelete.add(i);
        for(int i=indexListToDelete.size()-1;i>=0;i--){
            int currentIndexToDelete=indexListToDelete.get(i);
            orderAmountList.remove(currentIndexToDelete);
            detailCurrencyList.remove(currentIndexToDelete); //KENAPA??
        }
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(currentTime);
        infoKeyValueList=new ArrayList<>();
        infoKeyValueList.add(new InfoKeyValue("ID Number",sharedPref.getString("card_id","")));
        if(orderDelivery==2){
            infoKeyValueList.add(new InfoKeyValue("Date",formattedDate));
            infoKeyValueList.add(new InfoKeyValue("Name",sharedPref.getString("name","")));
            infoKeyValueList.add(new InfoKeyValue("Address",sharedPref.getString("address","")));
            OngkirCard.setVisibility(View.GONE);

        }
        else{
            OngkirCard.setVisibility(View.VISIBLE);
            orderTotal=orderTotal+Integer.valueOf(sharedPref.getString("ongkir",""));
            Ongkir.setText(numberFormat.format(Integer.valueOf(sharedPref.getString("ongkir",""))));
            infoKeyValueList.add(new InfoKeyValue("Recipient Name",sharedPref.getString("recipient","")));
            infoKeyValueList.add(new InfoKeyValue("Delivery Date",sharedPref.getString("deliver_date","")));
            infoKeyValueList.add(new InfoKeyValue("Delivery Address",sharedPref.getString("deliver_address","")));
            Integer optionDelivery=sharedPref.getInt("seal",0); //AMAN???
            String optionString=optionDelivery==0?"Uang Dihitungkan Oleh Penerima ":(optionDelivery==1?"Segel":"-");
            infoKeyValueList.add(new InfoKeyValue("Other Option",optionString));
//            Toast.makeText(getBaseContext(),optionString,Toast.LENGTH_LONG).show();
            payment.setText(sharedPref.getString("accountProvider","")+" "+sharedPref.getString("accountNumber","")+" a/n "+sharedPref.getString("accountName",""));


        }
        tvConfirmOrderTotal.setText(numberFormat.format(orderTotal));

        String deliveryString=orderDelivery==2?"Pick up in store":(orderDelivery==1?"Deliver to Home":"Unknown");
        infoKeyValueList.add(new InfoKeyValue("Delivery Method",deliveryString));

        RecyclerView.LayoutManager mLayoutManagerOrderConfirmation = new LinearLayoutManager(getApplicationContext());
        RecyclerView.LayoutManager mLayoutManagerDetailorderConfirmation = new LinearLayoutManager(getApplicationContext());
        rvOrderConfirmation=findViewById(R.id.rvOrderConfirmation);
        customOrderAdapter = new ConfirmOrderAdapter(infoKeyValueList);
        rvOrderConfirmation.setLayoutManager(mLayoutManagerOrderConfirmation);
        rvOrderConfirmation.setItemAnimator(new DefaultItemAnimator());
        rvOrderConfirmation.setAdapter(customOrderAdapter);
        rvDetailOrderConfirmation=findViewById(R.id.rvDetailOrderConfirmation);
        customDetailOrderAdapter = new ConfirmDetailOrderAdapter(detailCurrencyList,orderAmountList);
        rvDetailOrderConfirmation.setLayoutManager(mLayoutManagerDetailorderConfirmation);
        rvDetailOrderConfirmation.setItemAnimator(new DefaultItemAnimator());
        rvDetailOrderConfirmation.setAdapter(customDetailOrderAdapter);

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void ConfirmOrder(View view){
        spinner.setVisibility(View.VISIBLE);
        //JIKA SP HILANG, PERLU CEK API??
        String amountConcat="";
        String detailCurrencyIdConcat="";
        for(int i=0;i<detailCurrencyList.size();i++){
            amountConcat+=Integer.toString(orderAmountList.get(i));
            detailCurrencyIdConcat+=Integer.toString(detailCurrencyList.get(i).id);
            if(i!=detailCurrencyList.size()-1){
                amountConcat+="&";
                detailCurrencyIdConcat+="&";
            }
        }
        Call<CreatedOrder> createOrderCall = apiInterface.ProcessCreateOrder(new CreateOrderData(amountConcat,detailCurrencyIdConcat,orderTotal,Integer.parseInt(sharedPref.getString("id","")),orderDelivery,sharedPref.getString("deliver_address",""),sharedPref.getString("recipient",""),sharedPref.getString("deliver_date",""),sharedPref.getInt("seal",0)));
        createOrderCall.enqueue(new Callback<CreatedOrder>() {
            @Override
            public void onResponse(Call<CreatedOrder> call, Response<CreatedOrder> response) {
                if(response.code()==200) {
                    CreatedOrder apiRes = response.body();
                    Intent intentPayment = new Intent(ConfirmOrderActivity.this,DetailHistoryActivity.class);
                    intentPayment.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    Bundle bundle = new Bundle();
                    bundle.putInt("current_order_id",apiRes.order.id);
                    bundle.putBoolean("after_confirm_order",true);
                    bundle.putString("current_order_number",apiRes.order.orderNumber);
                    intentPayment.putExtras(bundle);
                    startActivity(intentPayment);
                }else if(response.code()==466){
                    BuildSimpleDialog(ConfirmOrderActivity.this.getString(R.string.error_title_insufficient_stock), ConfirmOrderActivity.this.getString(R.string.error_message_insufficient_stock), "Dismiss");
                }else if(response.code()==467){
                    BuildSimpleDialog(ConfirmOrderActivity.this.getString(R.string.error_title_price_differece), ConfirmOrderActivity.this.getString(R.string.error_message_price_differece), "Dismiss");
                }else if(response.code()==472){
                    BuildSimpleDialog(ConfirmOrderActivity.this.getString(R.string.error_title_closed_shop), ConfirmOrderActivity.this.getString(R.string.error_message_closed_shop), "Dismiss");
                }else if(response.code()==474){
                    BuildSimpleDialog(ConfirmOrderActivity.this.getString(R.string.error_title_pending_transaction), ConfirmOrderActivity.this.getString(R.string.error_message_pending_transaction), "Dismiss");
                }else if(response.code()==480){
                    BuildSimpleDialog(ConfirmOrderActivity.this.getString(R.string.error_title_unauthorized), ConfirmOrderActivity.this.getString(R.string.error_message_unauthorized), "Dismiss");
                }else{
                    BuildSimpleDialog(ConfirmOrderActivity.this.getString(R.string.error_title_server_error), ConfirmOrderActivity.this.getString(R.string.error_message_server_error), "Dismiss");
                }
                spinner.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<CreatedOrder> call, Throwable t) {
                call.cancel();
                spinner.setVisibility(View.GONE);
                BuildSimpleDialog(ConfirmOrderActivity.this.getString(R.string.error_title_internet), ConfirmOrderActivity.this.getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }

    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    public class ConfirmOrderAdapter extends RecyclerView.Adapter<ConfirmOrderAdapter.ConfirmOrderViewHolder> {

        private List<InfoKeyValue> adapterInfoKeyValueList;

        public class ConfirmOrderViewHolder extends RecyclerView.ViewHolder {
            public TextView tvConfirmOrderKey, tvConfirmOrderValue;

            public ConfirmOrderViewHolder(View view) {
                super(view);
                tvConfirmOrderKey = view.findViewById(R.id.tvConfirmOrderKey);
                tvConfirmOrderValue = view.findViewById(R.id.tvConfirmOrderValue);
            }
        }

        public ConfirmOrderAdapter(List<InfoKeyValue> adapterInfoKeyValueList) {
            this.adapterInfoKeyValueList = adapterInfoKeyValueList;
        }

        @Override
        public ConfirmOrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_confirmorder, parent, false);

            return new ConfirmOrderViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final ConfirmOrderViewHolder holder, final int position) {
            InfoKeyValue ifkRow = adapterInfoKeyValueList.get(position);
            holder.tvConfirmOrderKey.setText(ifkRow.infoKey);
            holder.tvConfirmOrderValue.setText(ifkRow.infoValue);
        }

        @Override
        public int getItemCount() {
            return adapterInfoKeyValueList.size();
        }
    }

    public class ConfirmDetailOrderAdapter extends RecyclerView.Adapter<ConfirmDetailOrderAdapter.ConfirmDetailOrderViewHolder> {

        private List<DetailCurrencyList.DetailCurrency> adapterConfirmDetailOrderList;
        private List<Integer> adapterOrderAmountList;

        public class ConfirmDetailOrderViewHolder extends RecyclerView.ViewHolder {
            public TextView tvConfirmDOAmountValue,tvRateAmountValue,tvConfirmDOSubtotal;

            public ConfirmDetailOrderViewHolder(View view) {
                super(view);
                tvConfirmDOAmountValue = view.findViewById(R.id.tvConfirmDOAmountValue);
                tvRateAmountValue = view.findViewById(R.id.tvRateAmountValue);
                tvConfirmDOSubtotal = view.findViewById(R.id.tvConfirmDOSubtotal);
            }
        }

        public ConfirmDetailOrderAdapter(List<DetailCurrencyList.DetailCurrency> adapterConfirmDetailOrderList, List<Integer> adapterOrderAmountList) {
            this.adapterConfirmDetailOrderList = adapterConfirmDetailOrderList;
            this.adapterOrderAmountList=adapterOrderAmountList;
        }

        @Override
        public ConfirmDetailOrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_confirmdetailorder, parent, false);

            return new ConfirmDetailOrderViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final ConfirmDetailOrderViewHolder holder, final int position) {
            DetailCurrencyList.DetailCurrency cdoRow = adapterConfirmDetailOrderList.get(position);
            holder.tvConfirmDOAmountValue.setText(Integer.toString(adapterOrderAmountList.get(position)) + " X " + cdoRow.codeCur + " " + Integer.toString(cdoRow.nominal));
            Float price=cdoRow.price;
            holder.tvRateAmountValue.setText(numberFormat.format(price));
            Float subTotal=cdoRow.price*adapterOrderAmountList.get(position)*cdoRow.nominal;
            holder.tvConfirmDOSubtotal.setText(numberFormat.format(subTotal));
        }

        @Override
        public int getItemCount() {
            return adapterConfirmDetailOrderList.size();
        }
    }
}
