package com.moneychanger.luxodev.moneychanger.pojo.response;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class RegisteredCustomer {
    @SerializedName("customer")
    public Customer customer;

    public class Customer {

        @SerializedName("id")
        public Integer id;
        @SerializedName("name")
        public String name;
        @SerializedName("imei")
        public String imei;
        @SerializedName("phone")
        public String phone;
        @SerializedName("card_id")
        public String cardId;
        @SerializedName("card_image")
        public String cardImage;
        @SerializedName("address")
        public String address;
        @Nullable
        @SerializedName("address2")
        public String address2;

        @SerializedName("status")
        public String status;
        @SerializedName("email")
        public String email;
    }
}
