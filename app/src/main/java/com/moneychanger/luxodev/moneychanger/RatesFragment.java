package com.moneychanger.luxodev.moneychanger;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.moneychanger.luxodev.moneychanger.pojo.response.CurrencyList;
import com.moneychanger.luxodev.moneychanger.pojo.response.Notif;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RatesFragment extends Fragment {

    APIInterface apiInterface;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    List<CurrencyList.Currency> currencyList;
    List<Notif.Notify> notifList;

    ListView listView;
    ListView listNotif;
    TextView lastupd,Notiforder,warn;
    CustomAdapterCurrency customAdapter;
    CustomAdapterNotif notifAdapter;
    SwipeRefreshLayout swipeRefresh;
    ProgressBar spinner;
    ImageView ivLogo;
    TableLayout tableLayout;
    Button btnToOrder;
    LinearLayout main;

    AlertDialog.Builder alertDialogBuilder;
    CardView cardClosedShop;
    Context mContext;

    Boolean is_closed;

    public static RatesFragment newInstance() {
        return new RatesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPref=getActivity().getSharedPreferences(getActivity().getString(R.string.app_name), Context.MODE_PRIVATE);
        editor=sharedPref.edit();
        mContext=getContext();
        alertDialogBuilder=new AlertDialog.Builder(getActivity());
        apiInterface = APIClient.getClient().create(APIInterface.class);
        currencyList=new ArrayList<>();
        notifList=new ArrayList<>();
        is_closed = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rates, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        ivLogo=getView().findViewById(R.id.ivLogo);
        tableLayout=(TableLayout) getView().findViewById(R.id.tableLayout);

        lastupd=getView().findViewById(R.id.lastup);
        warn=getView().findViewById(R.id.warn);
        Notiforder=getView().findViewById(R.id.alert);
        btnToOrder = getView().findViewById(R.id.BtnToOrder);

        cardClosedShop=getView().findViewById(R.id.cardClosedShop);
        main=getView().findViewById(R.id.mainLayout);
        cardClosedShop.setVisibility(View.GONE);
        listNotif=getView().findViewById(R.id.listNotif);
        customAdapter=new CustomAdapterCurrency();
        notifAdapter=new CustomAdapterNotif();
//        listView.setAdapter(customAdapter);
        listNotif.setAdapter(notifAdapter);

        spinner = getView().findViewById(R.id.progressBarRates);

        if(currencyList.size()==0){
            cardClosedShop.setVisibility(View.VISIBLE);
//            ivLogo.setVisibility(View.GONE);
//            lastupd.setVisibility(View.GONE);
//            btnToOrder.setVisibility(View.GONE);
            main.setVisibility(View.GONE);
            is_closed = true;
        }
        else{
            cardClosedShop.setVisibility(View.GONE);
//            ivLogo.setVisibility(View.VISIBLE);
            main.setVisibility(View.VISIBLE);

            is_closed = false;

        }
        String Last=sharedPref.getString("last","-");
        lastupd.setText("Last Update: "+Last);


//        listView= getView().findViewById(R.id.listview);

        spinner.setVisibility(View.GONE);
        btnToOrder.setTextSize(18);
        btnToOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),ConversionActivity.class);
                startActivity(intent);
            }
        });

        swipeRefresh = getView().findViewById(R.id.swipeRefreshCurrency);
        swipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        requestCurrencyList(true);
                    }
                }
        );

        requestCurrencyList(false);

    }

    public void requestCurrencyList(final boolean isRefresh){
        cardClosedShop.setVisibility(View.GONE);

        if(!isRefresh) spinner.setVisibility(View.VISIBLE);

        Call<CurrencyList> currencyCall = apiInterface.GetCurrencyList();
        currencyCall.enqueue(new Callback<CurrencyList>() {
            @Override
            public void onResponse(Call<CurrencyList> call, Response<CurrencyList> response) {
                if(response.code()==200) {
                    currencyList.clear();
                    DateFormat df = new SimpleDateFormat("EEE, d MMM yyyy, HH:mm");
                    String date = df.format(Calendar.getInstance().getTime());
                    editor.putString("last",date);
                    editor.commit();
                    lastupd.setText("Last Update: "+date);
                    CurrencyList apiRes = response.body();
                    currencyList = apiRes.currencyList;
                    customAdapter.notifyDataSetChanged();

                    //editor.putFloat("maximum_purchase")
                    BuildTable();
                    if(currencyList.size()==0){
                        cardClosedShop.setVisibility(View.VISIBLE);
//            ivLogo.setVisibility(View.GONE);
//            lastupd.setVisibility(View.GONE);
                        main.setVisibility(View.GONE);

                        is_closed = true;

                    }
                    else{
                        cardClosedShop.setVisibility(View.GONE);
//            ivLogo.setVisibility(View.VISIBLE);
                        main.setVisibility(View.VISIBLE);

                        is_closed = false;
                        requestNotifList(isRefresh);
                    }
                }else if(response.code()==480){
                    BuildSimpleDialog(getActivity().getString(R.string.error_title_unauthorized), getActivity().getString(R.string.error_message_unauthorized), "Dismiss");
                }else{
                    BuildSimpleDialog(getActivity().getString(R.string.error_title_server_error), getActivity().getString(R.string.error_message_server_error), "Dismiss");
                }
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<CurrencyList> call, Throwable t) {
                Log.d("onFailure", t.getMessage());
                call.cancel();
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
                BuildSimpleDialog(getActivity().getString(R.string.error_title_internet), getActivity().getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }
    public void BuildTable(){
        int count = tableLayout.getChildCount();
        for (int i = 1; i < count; i++) {
            View child = tableLayout.getChildAt(i);
            if (child instanceof TableRow) ((ViewGroup) child).removeAllViews();
        }
        for (int i =0;i<currencyList.size();i++){
            View tableRow = LayoutInflater.from(mContext).inflate(R.layout.tbl_item,null,false);
            TextView tdcode  = (TextView) tableRow.findViewById(R.id.tdcode);
            TextView tdbuy  = (TextView) tableRow.findViewById(R.id.tdbuy);
            TextView tdsell  = (TextView) tableRow.findViewById(R.id.tdsell);
            tdcode.setText(currencyList.get(i).code);
            if(currencyList.get(i).buy_range.toString().equals("-")){
                tdbuy.setGravity(Gravity.CENTER);
            }
            else{
                tdbuy.setGravity(Gravity.RIGHT);
            }
            tdbuy.setText(currencyList.get(i).buy_range);
            if(currencyList.get(i).range.toString().equals("-")){
                tdsell.setGravity(Gravity.CENTER);
            }
            else{
                tdsell.setGravity(Gravity.RIGHT);
            }
            tdsell.setText(currencyList.get(i).range);
            tableLayout.addView(tableRow);
        }
    }
    public void requestNotifList(final boolean isRefresh){
        cardClosedShop.setVisibility(View.GONE);

        if(!isRefresh) spinner.setVisibility(View.VISIBLE);
        Call<Notif> notifyCall = apiInterface.GetNotifyList(Integer.valueOf(sharedPref.getString("id","1")));
        notifyCall.enqueue(new Callback<Notif>() {
            @Override
            public void onResponse(Call<Notif> call, Response<Notif> response) {
                if(response.code()==200) {
                    notifList.clear();
                    final Notif apiRes = response.body();
//                    Toast.makeText(getContext(),response.body().notifList.get(0).notif,Toast.LENGTH_LONG).show();
                    notifList = apiRes.notifList;

                    if(Integer.valueOf(apiRes.listHistoryOrder.size())>0){
                        Notiforder.setVisibility(View.VISIBLE);
                        Notiforder.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intentDetailHistory = new Intent(getActivity(),DetailHistoryActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putInt("current_order_id", apiRes.listHistoryOrder.get(0).id);
                                bundle.putBoolean("after_confirm_order",false);
                                bundle.putString("current_order_number",apiRes.listHistoryOrder.get(0).orderNumber);
                                intentDetailHistory.putExtras(bundle);
                                startActivity(intentDetailHistory);
                            }
                        });

                    }
                    else{
                        if(!is_closed) {
                            main.setVisibility(View.VISIBLE);
                            cardClosedShop.setVisibility(View.GONE);
                        } else {
                            main.setVisibility(View.GONE);
                            cardClosedShop.setVisibility(View.VISIBLE);
                        }


//                        Notiforder.setVisibility(View.GONE);


                    }
                    notifAdapter.notifyDataSetChanged();
                }else if(response.code()==480){
                    BuildSimpleDialog(getActivity().getString(R.string.error_title_unauthorized), getActivity().getString(R.string.error_message_unauthorized), "Dismiss");
                }else{
                    BuildSimpleDialog(getActivity().getString(R.string.error_title_server_error), getActivity().getString(R.string.error_message_server_error), "Dismiss");
                }
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<Notif> call, Throwable t) {
                Log.d("onFailure", t.getMessage());
                call.cancel();
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
                BuildSimpleDialog(getActivity().getString(R.string.error_title_internet), getActivity().getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }

    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    class CustomAdapterCurrency extends BaseAdapter{

        @Override
        public int getCount() {
            return currencyList.size()>0 ? currencyList.size()+1 : 0;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view=getLayoutInflater().inflate(R.layout.listview_rates,null);

            if(currencyList.size()>0 && i==currencyList.size()) {
                view.findViewById(R.id.cardRates).setVisibility(View.GONE);
                Button btnToOrder = new Button(getActivity());
                btnToOrder.setText("Order");
                btnToOrder.setTextSize(18);

                btnToOrder.setTextColor(getResources().getColor(R.color.mcWhite));
                btnToOrder.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.mcPrimary)));
                RelativeLayout.LayoutParams rl = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                rl.addRule(RelativeLayout.CENTER_HORIZONTAL);
                rl.addRule(RelativeLayout.CENTER_VERTICAL);
                rl.addRule(RelativeLayout.ALIGN_PARENT_START);
                rl.addRule(RelativeLayout.ALIGN_PARENT_END);
                rl.setMargins(16, 0, 16, 0);
                btnToOrder.setLayoutParams(rl);
                btnToOrder.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(),ConversionActivity.class);
                        startActivity(intent);
                    }
                });
                ((RelativeLayout) view.findViewById(R.id.layoutRates)).addView(btnToOrder);
            }else{
                TextView textCode=view.findViewById(R.id.textCode);
                TextView textName=view.findViewById(R.id.textName);
                TextView textPrice=view.findViewById(R.id.textPrice);
                TextView textPriceBuy=view.findViewById(R.id.textPriceBuy);
                textCode.setText(currencyList.get(i).code);
                textName.setText(currencyList.get(i).name);
                textPrice.setText(currencyList.get(i).range);
                textPriceBuy.setText(currencyList.get(i).buy_range);
            }

            return view;
        }

    }

    class CustomAdapterNotif extends BaseAdapter{

        @Override
        public int getCount() {
            return notifList.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.recyclerview_news, null);

            try {
                TextView textNews = view.findViewById(R.id.tvMainNews);
                TextView textDate = view.findViewById(R.id.tvDate);
                textNews.setText(notifList.get(i).notif);
                textDate.setText(notifList.get(i).createdAt);

            }
            catch (Exception e){

            }
            return view;

        }

    }
}
