package com.moneychanger.luxodev.moneychanger.pojo.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eka wijaya susilo on 3/30/2018.
 */

public class LoginData {
    @SerializedName("customer_id")
    public Integer customerId;
    @SerializedName("imei")
    public String imei;

    public LoginData(Integer customerId, String imei){
        this.customerId=customerId;
        this.imei=imei;
    }
}
