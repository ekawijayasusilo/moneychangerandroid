package com.moneychanger.luxodev.moneychanger.pojo.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OrderList {

    @SerializedName("listHistoryOrder")
    public List<Order> listHistoryOrder = new ArrayList();

    public class Order {
        @SerializedName("detail_order")
        public List<DetailOrder> listHistoryDetailOrder = new ArrayList();

        @SerializedName("id")
        public Integer id;
        @SerializedName("order_number")
        public String orderNumber;
        @SerializedName("customer_id")
        public Integer customerId;
        @SerializedName("courier_id")
        public Integer courierId;
        @SerializedName("total")
        public Float total;
        @SerializedName("status")
        public Integer status;
        @SerializedName("delivery")
        public Integer delivery;
        @SerializedName("receiver_name")
        public String receiverName;
        @SerializedName("image_receipt")
        public String imageReceipt;
        @SerializedName("latitude")
        public Float latitude;
        @SerializedName("longitude")
        public Float longitude;
        @SerializedName("created_at")
        public String createdAt;
        @SerializedName("updated_at")
        public String updatedAt;
        @SerializedName("date")
        public String date;
        @SerializedName("status_name")
        public String statusName;
        @SerializedName("delivery_name")
        public String deliveryName;
        @SerializedName("total_nominal")
        public String totalNominal;
        @SerializedName("deliver_address")
        public String deliverAddress;
        @SerializedName("deliv_date")
        public String deliverDate;
        @SerializedName("ongkir")
        public String ongkir;
        @SerializedName("region")
        public String region;
        @SerializedName("seal")
        public String seal;
        @SerializedName("confirmable")
        public Integer confirmable;


        public class DetailOrder {
            @SerializedName("code")
            public String code;
            @SerializedName("amount")
            public String amount;
            @SerializedName("price")
            public String price;
            @SerializedName("total")
            public String total;
        }
    }
}
