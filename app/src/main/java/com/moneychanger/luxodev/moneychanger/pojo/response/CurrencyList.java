package com.moneychanger.luxodev.moneychanger.pojo.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CurrencyList {
    @SerializedName("currencyList")
    public List<Currency> currencyList = new ArrayList();

    public class Currency {

        @SerializedName("id")
        public Integer id;
        @SerializedName("name")
        public String name;
        @SerializedName("code")
        public String code;
        @SerializedName("total_stock")
        public Integer totalStock;
        @SerializedName("range")
        public String range;
        @SerializedName("buy_range")
        public String buy_range;
        @SerializedName("created_at")
        public String createdAt;
        @SerializedName("updated_at")
        public String updatedAt;

    }
}
