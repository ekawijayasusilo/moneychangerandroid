package com.moneychanger.luxodev.moneychanger;

import com.moneychanger.luxodev.moneychanger.pojo.request.CreateOrderData;
import com.moneychanger.luxodev.moneychanger.pojo.request.LoginData;
import com.moneychanger.luxodev.moneychanger.pojo.request.RegisterCustomer;
import com.moneychanger.luxodev.moneychanger.pojo.response.CreatedOrder;
import com.moneychanger.luxodev.moneychanger.pojo.response.CurrencyList;
import com.moneychanger.luxodev.moneychanger.pojo.request.RegisterPhoneOTP;
import com.moneychanger.luxodev.moneychanger.pojo.response.DetailCurrencyList;
import com.moneychanger.luxodev.moneychanger.pojo.response.DetailOrderHistoryData;
import com.moneychanger.luxodev.moneychanger.pojo.response.Notif;
import com.moneychanger.luxodev.moneychanger.pojo.response.OTPCode;
import com.moneychanger.luxodev.moneychanger.pojo.response.Ongkir;
import com.moneychanger.luxodev.moneychanger.pojo.response.OrderList;
import com.moneychanger.luxodev.moneychanger.pojo.response.RegisteredCustomer;

import retrofit2.Call;
//import retrofit2.http.Body;
//import retrofit2.http.Field;
//import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

interface APIInterface {

    @POST("/api/customer/register/otp")
    Call<OTPCode> RegisterPhoneOTP(@Body RegisterPhoneOTP registerPhoneOTP);

    @POST("/api/customer/register")
    Call<RegisteredCustomer> RegisterCustomerData(@Body RegisterCustomer registerCustomer);

    @GET("/api/customer/login/approval/{id}")
    Call<Void> GetUserApproval(@Path("id") String id);

    @POST("/api/customer/login")
    Call<Void> ProcessLogin(@Body LoginData loginData);

    @GET("/api/customer/currency")
    Call<CurrencyList> GetCurrencyList();

    @GET("/api/get/notification?")
    Call<Notif> GetNotifyList(@Query("id") Integer customer_id);

    @GET("/api/ongkir?")
    Call<Ongkir> GetOngkirList(@Query("customer_id") Integer customer_id);

    @GET("/api/customer/currency/{id_customer}")
    Call<DetailCurrencyList> GetDetailCurrencyList(@Path("id_customer") String idCustomer);

    @GET("/api/customer/history?")
    Call<OrderList> GetOrderList(@Query("customer_id") Integer customer_id, @Query("status") Integer status);

    @POST("/api/customer/order")
    Call<CreatedOrder> ProcessCreateOrder(@Body CreateOrderData createOrderData);

    @POST("/api/customer/confirm_history/{order_id}")
    Call<DetailOrderHistoryData> ConfirmeOrder(@Path("order_id") String orderId);


    @GET("/api/customer/history/{order_id}")
    Call<DetailOrderHistoryData> GetDetailOrderHistoryData(@Path("order_id") String orderId);

    @GET("/api/customer/profile/{customer_id}?")
    Call<RegisteredCustomer> GetProfileData(@Path("customer_id") String id, @Query("hash") String imageHash);
}
