package com.moneychanger.luxodev.moneychanger;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.moneychanger.luxodev.moneychanger.pojo.response.DetailCurrencyList;
import com.moneychanger.luxodev.moneychanger.pojo.response.DetailOrderHistoryData;
import com.moneychanger.luxodev.moneychanger.pojo.response.OrderList;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.media.MediaExtractor.MetricsConstants.FORMAT;

public class DetailHistoryActivity extends AppCompatActivity {

    boolean afterConfirmOrder;
    Integer orderId;
//    List<InfoKeyValue> orderInfoList;
//    List<InfoKeyValue> paymentInfoList;

    SwipeRefreshLayout swipeRefresh;
    ProgressBar spinner;

    SharedPreferences sharedPref;
    APIInterface apiInterface;
    AlertDialog.Builder alertDialogBuilder;
    Locale localeID;
    NumberFormat numberFormat;

    RecyclerView rvOrderInfo,rvDeliverInfo,rvPaymentInfo;
    DeliverInfoAdapter customDeliverInfoAdapter;
    OrderInfoAdapter customOrderInfoAdapter;
    PaymentInfoAdapter customPaymentInfoAdapter;
    CountDownTimer cTimer = null;
    TextView tick,tvexpired,tvpayment,tvTimerInfo,alert,tvtotal,tvicon;
    CardView Time,PayInfo,Detail,CardDeliverInfo,CardOrderInfo;
    Button ConfirmPayment;
    LinearLayout totalLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_history);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle=getIntent().getExtras();
        orderId=bundle.getInt("current_order_id",0);
        afterConfirmOrder=bundle.getBoolean("after_confirm_order",false);
        getSupportActionBar().setTitle(bundle.getString("current_order_number",""));
//        ActionBar toolbar =getSupportActionBar();
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.mcPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        CardDeliverInfo=(CardView) findViewById(R.id.cardDeliverInfo);
        CardDeliverInfo.setVisibility(View.GONE);
        CardOrderInfo=(CardView) findViewById(R.id.cardOrderInfo);
        CardOrderInfo.setVisibility(View.GONE);

        sharedPref = getSharedPreferences(DetailHistoryActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
        localeID = new Locale("in", "ID");
        numberFormat = NumberFormat.getCurrencyInstance(localeID);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        alertDialogBuilder=new AlertDialog.Builder(DetailHistoryActivity.this);
        tick = (TextView) findViewById(R.id.tick);
        tvexpired=(TextView) findViewById(R.id.tvExpired);
        tvpayment=(TextView) findViewById(R.id.tvPayment);
        tvtotal=(TextView) findViewById(R.id.tvtotal);

        totalLayout=(LinearLayout) findViewById(R.id.layoutTotal);

        alert=(TextView) findViewById(R.id.alert);
//        orderInfoList=new ArrayList<>();
//        paymentInfoList=new ArrayList<>();
        alert.setVisibility(View.GONE);
        RecyclerView.LayoutManager mLayoutManagerOrderInfo = new LinearLayoutManager(getApplicationContext());
        RecyclerView.LayoutManager mLayoutManagerPaymentInfo = new LinearLayoutManager(getApplicationContext());
        RecyclerView.LayoutManager mLayoutManagerdeliverInfo = new LinearLayoutManager(getApplicationContext());
        rvOrderInfo=findViewById(R.id.rvOrderInfo);
        customOrderInfoAdapter = new OrderInfoAdapter(new ArrayList<InfoKeyValue>());
        rvOrderInfo.setLayoutManager(mLayoutManagerOrderInfo);
        rvOrderInfo.setItemAnimator(new DefaultItemAnimator());
        rvOrderInfo.setAdapter(customOrderInfoAdapter);

        rvDeliverInfo=findViewById(R.id.rvDeliverInfo);
        customDeliverInfoAdapter = new DeliverInfoAdapter(new ArrayList<InfoKeyValue>());
        rvDeliverInfo.setLayoutManager(mLayoutManagerdeliverInfo);
        rvDeliverInfo.setItemAnimator(new DefaultItemAnimator());
        rvDeliverInfo.setAdapter(customDeliverInfoAdapter);

        rvPaymentInfo=findViewById(R.id.rvDetailInfo);
        customPaymentInfoAdapter = new PaymentInfoAdapter(new ArrayList<InfoKeyValue>());
        rvPaymentInfo.setLayoutManager(mLayoutManagerPaymentInfo);
        rvPaymentInfo.setItemAnimator(new DefaultItemAnimator());
        rvPaymentInfo.setAdapter(customPaymentInfoAdapter);
        tvTimerInfo=(TextView) findViewById(R.id.timerTitle);
        tvicon=(TextView) findViewById(R.id.tvicon);
        PayInfo=(CardView) findViewById(R.id.cardOrderConfirmation);
        Detail=(CardView) findViewById(R.id.rvPaymentInfo);
        Time =(CardView) findViewById(R.id.cardTimeInfo);
        ConfirmPayment=(Button) findViewById(R.id.btnconfirmpayment);
        ConfirmPayment.setVisibility(View.GONE);
        Time.setVisibility(View.GONE);
        PayInfo.setVisibility(View.GONE);
        Detail.setVisibility(View.GONE);
        ConfirmPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmOrder(true);

            }
        });

        spinner = findViewById(R.id.progressBarOrderInfo);
        spinner.setVisibility(View.GONE);
        swipeRefresh=findViewById(R.id.swipeRefreshOrderInfo);
        swipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        RequestDetailOrderHistory(true);
                    }
                }
        );

        RequestDetailOrderHistory(false);

    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        DeterminePrevActivity();
    }



    /**
     * onClick handler
     */
    public void toggle_contents(View v){
//        Animation a = AnimationUtils.loadAnimation(this, R.anim.slide_up);

        if(rvPaymentInfo.isShown()){
            rvPaymentInfo.setVisibility(View.GONE);
            tvicon.setBackground(ContextCompat.getDrawable(getBaseContext(),R.drawable.ic_keyboard_arrow_down_black_24dp));
        }
        else{
            rvPaymentInfo.setVisibility(View.VISIBLE);
            tvicon.setBackground(ContextCompat.getDrawable(getBaseContext(),R.drawable.ic_keyboard_arrow_up_black_24dp));

        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        DeterminePrevActivity();
        return true;
    }

    public void DeterminePrevActivity(){
        if(afterConfirmOrder){
            Intent intentMain = new Intent(DetailHistoryActivity.this,MainActivity.class);
            intentMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle bundle = new Bundle();
            bundle.putBoolean("logged",true);
            intentMain.putExtras(bundle);
            startActivity(intentMain);
        }else finish();
    }
    public void ConfirmOrder(final boolean isRefresh){
        if(!isRefresh) spinner.setVisibility(View.VISIBLE);
        Call<DetailOrderHistoryData> detailOrderHistoryDataCall = apiInterface.ConfirmeOrder(Integer.toString(orderId));
        detailOrderHistoryDataCall.enqueue(new Callback<DetailOrderHistoryData>() {
            @Override
            public void onResponse(Call<DetailOrderHistoryData> call, Response<DetailOrderHistoryData> response) {
                if(response.code()==200) {
                    BuildSimpleDialog("Terimakasih", "Terimakasih Telah mengkonfirmasi! Mohon menunggu sistem kami untuk melakukan pengecekan pada pembayaran anda!", "Dismiss");

                    DetailOrderHistoryData apiRes=response.body();
                    cancelTimer();
                    if(apiRes.order.status>=2){
                        Time.setVisibility(View.GONE);
                        Detail.setVisibility(View.VISIBLE);
                        PayInfo.setVisibility(View.GONE);

                    }
                    else{
                        if(apiRes.order.confirmable==2){
                            alert.setVisibility(View.VISIBLE);
                        }
                        else{
                            alert.setVisibility(View.GONE);
                        }
                        ConfirmPayment.setVisibility(View.GONE);
                        Detail.setVisibility(View.VISIBLE);
                        PayInfo.setVisibility(View.VISIBLE);
                        Time.setVisibility(View.VISIBLE);
                        tvpayment.setText(apiRes.accountProvider+" "+apiRes.accountNumber+" A/N "+apiRes.accountName);
                        tvexpired.setText(apiRes.expiredDate);
                        if(apiRes.remainingTime.equals("0")){
                            tick.setText("Expired");
                            tick.setTextColor(getResources().getColor(android.R.color.holo_red_light));
                        }
                        else{
                            startTimer(Integer.parseInt(apiRes.remainingTime));
                            tick.setTextColor(getResources().getColor(android.R.color.black));

                        }
                    }

                    customDeliverInfoAdapter.UpdateDataSet(BuildDeliverInfoList(apiRes.order, apiRes.order.status));
                    customOrderInfoAdapter.UpdateDataSet(BuildOrderInfoList(apiRes.order, apiRes.order.status));
                    customPaymentInfoAdapter.UpdateDataSet(BuildPaymentInfoList(apiRes.order.listHistoryDetailOrder,apiRes.order));
                }else if(response.code()==480){
                    BuildSimpleDialog(DetailHistoryActivity.this.getString(R.string.error_title_unauthorized), DetailHistoryActivity.this.getString(R.string.error_message_unauthorized), "Dismiss");
                }else{
                    BuildSimpleDialog(DetailHistoryActivity.this.getString(R.string.error_title_server_error), DetailHistoryActivity.this.getString(R.string.error_message_server_error), "Dismiss");
                }
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<DetailOrderHistoryData> call, Throwable t) {
                call.cancel();
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
                BuildSimpleDialog(DetailHistoryActivity.this.getString(R.string.error_title_internet), DetailHistoryActivity.this.getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }

    public void RequestDetailOrderHistory(final boolean isRefresh){
        if(!isRefresh) spinner.setVisibility(View.VISIBLE);
        Call<DetailOrderHistoryData> detailOrderHistoryDataCall = apiInterface.GetDetailOrderHistoryData(Integer.toString(orderId));
        detailOrderHistoryDataCall.enqueue(new Callback<DetailOrderHistoryData>() {
            @Override
            public void onResponse(Call<DetailOrderHistoryData> call, Response<DetailOrderHistoryData> response) {
                if(response.code()==200) {
                    DetailOrderHistoryData apiRes=response.body();
                    cancelTimer();
                    CardOrderInfo.setVisibility(View.VISIBLE);
                            CardDeliverInfo.setVisibility(View.VISIBLE);

                        if(apiRes.order.status>=2){
                        Time.setVisibility(View.GONE);
                        Detail.setVisibility(View.VISIBLE);
                        PayInfo.setVisibility(View.GONE);
                        ConfirmPayment.setVisibility(View.GONE);

                    }
                    else{


//                        Log.w("wow", "onResponse: "+apiRes.order.confirmable );
                        if(apiRes.order.confirmable.toString().equals("1")) {
                            ConfirmPayment.setVisibility(View.VISIBLE);
                            Log.w("wow", "onfi: "+apiRes.order.confirmable );
                        }
                        else{
                            ConfirmPayment.setVisibility(View.GONE);
                        }

                        if(apiRes.order.confirmable==2){
                            alert.setVisibility(View.VISIBLE);
                            ConfirmPayment.setVisibility(View.VISIBLE);
                        }
                        else{
                            alert.setVisibility(View.GONE);
                        }

                        Detail.setVisibility(View.VISIBLE);
                        PayInfo.setVisibility(View.VISIBLE);
                        Time.setVisibility(View.VISIBLE);
//                        tvtotal.setText(numberFormat.format(apiRes.order.total));
                        tvpayment.setText(apiRes.accountProvider+" "+apiRes.accountNumber+" A/N "+apiRes.accountName);
                        tvexpired.setText(apiRes.expiredDate);
                        if(apiRes.remainingTime.equals("0")){
                            tick.setText("Expired");
                            tick.setTextColor(getResources().getColor(android.R.color.holo_red_light));
                        }
                        else{
                            startTimer(Integer.parseInt(apiRes.remainingTime));
                            tick.setTextColor(getResources().getColor(android.R.color.black));

                        }
                    }
                    tvtotal.setText(numberFormat.format(apiRes.order.total));

                    customDeliverInfoAdapter.UpdateDataSet(BuildDeliverInfoList(apiRes.order, apiRes.order.status));
                    customOrderInfoAdapter.UpdateDataSet(BuildOrderInfoList(apiRes.order, apiRes.order.status));
                    customPaymentInfoAdapter.UpdateDataSet(BuildPaymentInfoList(apiRes.order.listHistoryDetailOrder,apiRes.order));
                }else if(response.code()==480){
                    BuildSimpleDialog(DetailHistoryActivity.this.getString(R.string.error_title_unauthorized), DetailHistoryActivity.this.getString(R.string.error_message_unauthorized), "Dismiss");
                }else{
                    BuildSimpleDialog(DetailHistoryActivity.this.getString(R.string.error_title_server_error), DetailHistoryActivity.this.getString(R.string.error_message_server_error), "Dismiss");
                }
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<DetailOrderHistoryData> call, Throwable t) {
                call.cancel();
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
                BuildSimpleDialog(DetailHistoryActivity.this.getString(R.string.error_title_internet), DetailHistoryActivity.this.getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }

    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public List<InfoKeyValue> BuildPaymentInfoList(List<OrderList.Order.DetailOrder> Detail, OrderList.Order Ord){

        List<InfoKeyValue> ifkList=new ArrayList<>();
        for(int i=0;i<Detail.size();i++){
            ifkList.add(new InfoKeyValue(Detail.get(i).code+"@"+numberFormat.format(Double.parseDouble(Detail.get(i).price))+" x "+Detail.get(i).amount, numberFormat.format(Double.parseDouble(Detail.get(i).total))));

        }
//            ifkList.add(new InfoKeyValue("Remaining Time", remainingTime));
        if(Ord.delivery==1){
            try {
                ifkList.add(new InfoKeyValue("Ongkir",numberFormat.format(Double.parseDouble(Ord.ongkir))));

            }
            catch (Exception e){

            }

        }
        return ifkList;
    }


    public List<InfoKeyValue> BuildOrderInfoList(OrderList.Order order, Integer listMode){
        List<InfoKeyValue> ifkList=new ArrayList<>();
        ifkList.add(new InfoKeyValue("Order Number", order.orderNumber));
        ifkList.add(new InfoKeyValue("Status", order.statusName));
        ifkList.add(new InfoKeyValue("Date", order.date));

        return ifkList;
    }

    public List<InfoKeyValue> BuildDeliverInfoList(OrderList.Order order, Integer listMode){
        List<InfoKeyValue> ifkList=new ArrayList<>();
        if(order.delivery==2){
            ifkList.add(new InfoKeyValue("Name", sharedPref.getString("name","")));
            ifkList.add(new InfoKeyValue("ID Number", sharedPref.getString("card_id","")));
            //ifkList.add(new InfoKeyValue("Total Nominal", order.totalNominal));
//            ifkList.add(new InfoKeyValue("Address", sharedPref.getString("address","")));
            if(listMode==4){
                ifkList.add(new InfoKeyValue("Receiver", order.receiverName));
            }
        }
        else{
            ifkList.add(new InfoKeyValue("Receiver", order.receiverName));
            ifkList.add(new InfoKeyValue("Address", order.deliverAddress));
            ifkList.add(new InfoKeyValue("Deliver Date", order.deliverDate));
            ifkList.add(new InfoKeyValue("Other Options", order.seal));

        }

        ifkList.add(new InfoKeyValue("Delivery", order.deliveryName));

        return ifkList;
    }

    public class OrderInfoAdapter extends RecyclerView.Adapter<OrderInfoAdapter.OrderInfoViewHolder> {

        private List<InfoKeyValue> adapterInfoKeyValueListOrder;

        public class OrderInfoViewHolder extends RecyclerView.ViewHolder {
            public TextView tvConfirmOrderKey, tvConfirmOrderValue;

            public OrderInfoViewHolder(View view) {
                super(view);
                tvConfirmOrderKey = view.findViewById(R.id.tvConfirmOrderKey);
                tvConfirmOrderValue = view.findViewById(R.id.tvConfirmOrderValue);
            }
        }

        public void UpdateDataSet(List<InfoKeyValue> adapterInfoKeyValueListOrder){
            this.adapterInfoKeyValueListOrder.clear();
            this.adapterInfoKeyValueListOrder = adapterInfoKeyValueListOrder;
            this.notifyDataSetChanged();
        }

        public OrderInfoAdapter(List<InfoKeyValue> adapterInfoKeyValueListOrder) {
            this.adapterInfoKeyValueListOrder = adapterInfoKeyValueListOrder;
        }

        @Override
        public OrderInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_confirmordercurrency, parent, false);
            return new OrderInfoViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final OrderInfoViewHolder holder, final int position) {
            InfoKeyValue ifkRow = adapterInfoKeyValueListOrder.get(position);
            holder.tvConfirmOrderKey.setText(ifkRow.infoKey);
            if(ifkRow.infoValue.equals("Status")){
                holder.tvConfirmOrderValue.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.mcPrimary));
            }
            holder.tvConfirmOrderValue.setText(ifkRow.infoValue);
        }

        @Override
        public int getItemCount() {
            return this.adapterInfoKeyValueListOrder.size();
        }
    }


    public class DeliverInfoAdapter extends RecyclerView.Adapter<DeliverInfoAdapter.DeliverInfoViewHolder> {

        private List<InfoKeyValue> adapterInfoKeyValueListOrder;

        public class DeliverInfoViewHolder extends RecyclerView.ViewHolder {
            public TextView tvConfirmOrderKey, tvConfirmOrderValue;

            public DeliverInfoViewHolder(View view) {
                super(view);
                tvConfirmOrderKey = view.findViewById(R.id.tvConfirmOrderKey);
                tvConfirmOrderValue = view.findViewById(R.id.tvConfirmOrderValue);
            }
        }

        public void UpdateDataSet(List<InfoKeyValue> adapterInfoKeyValueListOrder){
            this.adapterInfoKeyValueListOrder.clear();
            this.adapterInfoKeyValueListOrder = adapterInfoKeyValueListOrder;
            this.notifyDataSetChanged();
        }

        public DeliverInfoAdapter(List<InfoKeyValue> adapterInfoKeyValueListOrder) {
            this.adapterInfoKeyValueListOrder = adapterInfoKeyValueListOrder;
        }

        @Override
        public DeliverInfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_confirmordercurrency, parent, false);
            return new DeliverInfoViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final DeliverInfoViewHolder holder, final int position) {
            InfoKeyValue ifkRow = adapterInfoKeyValueListOrder.get(position);
            holder.tvConfirmOrderKey.setText(ifkRow.infoKey);
            holder.tvConfirmOrderValue.setText(ifkRow.infoValue);
        }

        @Override
        public int getItemCount() {
            return this.adapterInfoKeyValueListOrder.size();
        }
    }

    public class PaymentInfoAdapter extends RecyclerView.Adapter<PaymentInfoAdapter.PaymentViewHolder> {

        private List<InfoKeyValue> adapterInfoKeyValueListPayment;

        @Override
        public PaymentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_confirmordercurrency, parent, false);

            return new PaymentViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final PaymentViewHolder holder, final int position) {
            InfoKeyValue ifkRow = adapterInfoKeyValueListPayment.get(position);
            if(ifkRow.infoKey.equals("Ongkir")){
                holder.main.setBackground(ContextCompat.getDrawable(getBaseContext(),R.drawable.underhoodnigga));
            }
            else{
                holder.main.setBackground(ContextCompat.getDrawable(getBaseContext(),R.drawable.underhoodlite));
            }
            holder.tvConfirmOrderKey.setText(ifkRow.infoKey);
            holder.tvConfirmOrderValue.setText(ifkRow.infoValue);
//            if(ifkRow.infoKey.equals("Remaining Time")) holder.tvPaymentInfoValue.setTextColor(getResources().getColor(android.R.color.holo_red_light));
        }

        public class PaymentViewHolder extends RecyclerView.ViewHolder {
            public TextView tvConfirmOrderKey, tvConfirmOrderValue;
            public RelativeLayout main;

            public PaymentViewHolder(View view) {
                super(view);
                main = view.findViewById(R.id.mainLayout);
                tvConfirmOrderKey = view.findViewById(R.id.tvConfirmOrderKey);
                tvConfirmOrderValue = view.findViewById(R.id.tvConfirmOrderValue);
            }
        }

        public void UpdateDataSet(List<InfoKeyValue> adapterInfoKeyValueListPayment){
            this.adapterInfoKeyValueListPayment.clear();
            this.adapterInfoKeyValueListPayment = adapterInfoKeyValueListPayment;
            this.notifyDataSetChanged();
        }

        public PaymentInfoAdapter(List<InfoKeyValue> adapterInfoKeyValueListPayment) {
            this.adapterInfoKeyValueListPayment = adapterInfoKeyValueListPayment;
        }



        @Override
        public int getItemCount() {
            return adapterInfoKeyValueListPayment.size();
        }
    }

    void startTimer(int time) {
        time=time*1000;
        cTimer = new CountDownTimer(time, 1000) {
            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000) % 60 ;
                int minutes = (int) ((millisUntilFinished / (1000*60)) % 60);
                int hours   = (int) ((millisUntilFinished / (1000*60*60)) % 24);
                String text="%02d : %02d";

                if(hours>0){
                    text="%02d : %02d : %02d";
                    tick.setText(String.format(text,
                            hours,minutes,seconds)
                    );
                }
                else{
                    tick.setText(String.format(text,
                            minutes,seconds)
                    );
                }


            }
            public void onFinish() {
                tick.setText("Expired");
                tick.setTextColor(getResources().getColor(android.R.color.holo_red_light));

            }
        };
        cTimer.start();
    }
    //cancel timer
    void cancelTimer() {
        if(cTimer!=null)
            cTimer.cancel();
    }



}

