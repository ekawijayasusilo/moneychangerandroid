package com.moneychanger.luxodev.moneychanger;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.moneychanger.luxodev.moneychanger.pojo.request.LoginData;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RuntimePermissions
public class LoginActivity extends AppCompatActivity {

    SharedPreferences sharedPref;
    APIInterface apiInterface;
    AlertDialog.Builder alertDialogBuilder;

    ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        sharedPref=getSharedPreferences(LoginActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
        alertDialogBuilder=new AlertDialog.Builder(LoginActivity.this);

        spinner = findViewById(R.id.progressBarLogin);
        spinner.setVisibility(View.GONE);

        TextView tvName=findViewById(R.id.tvName);
        tvName.setText(sharedPref.getString("name",null));
    }

    public void CheckPassword(View view){
        EditText etLogin=findViewById(R.id.etLogin);
        if(etLogin.getText().toString().equals(sharedPref.getString("password",null))){
            LoginActivityPermissionsDispatcher.GetImeiWithPermissionCheck(this);
        }else{
            BuildSimpleDialog(LoginActivity.this.getString(R.string.error_title_login),LoginActivity.this.getString(R.string.error_message_login),"Ok");
        }
    }
    public void Login(String imeiNumber) {
        spinner.setVisibility(View.VISIBLE);
        Call<Void> loginCall = apiInterface.ProcessLogin(new LoginData(Integer.parseInt(sharedPref.getString("id",null)), imeiNumber));
        loginCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                spinner.setVisibility(View.GONE);
                if(response.code()==200){
                    Intent intentMain = new Intent(LoginActivity.this,MainActivity.class);
                    intentMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentMain);
                }else if(response.code()==460 || response.code()==462){
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.remove("id");
                    editor.remove("name");
                    editor.remove("password");
                    editor.remove("phone");
                    editor.remove("card_id");
                    editor.remove("address");
                    editor.remove("otp_code");
                    editor.apply();

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setTitle(R.string.error_title_other_device);
                    if(response.code()==460) alertDialog.setMessage(LoginActivity.this.getString(R.string.error_message_other_device));
                    else alertDialog.setMessage(LoginActivity.this.getString(R.string.error_message_account_suspended));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Dismiss",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    Intent intentRegister = new Intent(LoginActivity.this,RegisterActivity.class);
                                    intentRegister.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intentRegister);
                                }
                            });
                    alertDialog.show();
                }else if(response.code()==480){
                    BuildSimpleDialog(LoginActivity.this.getString(R.string.error_title_unauthorized), LoginActivity.this.getString(R.string.error_message_unauthorized), "Dismiss");
                }else{
                    BuildSimpleDialog(LoginActivity.this.getString(R.string.error_title_server_error), LoginActivity.this.getString(R.string.error_message_server_error), "Dismiss");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                call.cancel();
                spinner.setVisibility(View.GONE);
                BuildSimpleDialog(LoginActivity.this.getString(R.string.error_title_internet), LoginActivity.this.getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }

    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @NeedsPermission(Manifest.permission.READ_PHONE_STATE)
    public void GetImei(){
        TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        @SuppressLint("MissingPermission") String possibleImei = telephonyManager.getDeviceId();
        if (possibleImei != null && !possibleImei.isEmpty()) {
            Login(possibleImei);
        } else {
            Login(android.os.Build.SERIAL);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        LoginActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(Manifest.permission.READ_PHONE_STATE)
    void showRationaleForReadState(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_read_state_rationale)
                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.READ_PHONE_STATE)
    void showDeniedForReadState() {
        Toast.makeText(this, R.string.permission_read_state_login_denied, Toast.LENGTH_SHORT).show();
    }
}
