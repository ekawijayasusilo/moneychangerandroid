package com.moneychanger.luxodev.moneychanger.pojo.response;

import com.google.gson.annotations.SerializedName;

public class OTPCode {
    @SerializedName("otp_code")
    public String otpCode;
}
