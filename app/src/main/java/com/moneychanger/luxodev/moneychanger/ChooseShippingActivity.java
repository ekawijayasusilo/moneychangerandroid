package com.moneychanger.luxodev.moneychanger;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.moneychanger.luxodev.moneychanger.pojo.request.CreateOrderData;
import com.moneychanger.luxodev.moneychanger.pojo.response.CreatedOrder;
import com.moneychanger.luxodev.moneychanger.pojo.response.Ongkir;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChooseShippingActivity extends AppCompatActivity {

    RadioGroup rgDeliveryOption,rgSealOption;
    private TextView mDates,eName;
    private Calendar calendar;
    Button btnChooseDelivery;
    SharedPreferences sharedPref;
    APIInterface apiInterface;
    AlertDialog.Builder alertDialogBuilder;
    ArrayList<String> MasterOngkir;
    List<String> categories;
    ProgressBar loader;
    ConstraintLayout main;

    private int year, month, day,mHour,mMinute;
    private Context mContext;
    SharedPreferences.Editor editor;
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0,
                              int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2+1, arg3);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_shipping);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.mcPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        sharedPref = getSharedPreferences(ChooseShippingActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        rgSealOption = (RadioGroup) findViewById(R.id.rgSealOption);
        rgDeliveryOption=findViewById(R.id.rgDeliveryOption);
        btnChooseDelivery=findViewById(R.id.btnDeliveryOption);
        final TextInputLayout input_layout = (TextInputLayout) findViewById(R.id.textInputLayout);
        final LinearLayout linear_layout=(LinearLayout) findViewById(R.id.linearLayout);
        loader = findViewById(R.id.progressBarAddress);
        main = (ConstraintLayout) findViewById(R.id.mainl);
        main.setVisibility(View.GONE);
        mDates = (TextView) findViewById(R.id.edate);
        eName = (TextView) findViewById(R.id.etName);
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        mContext =this;
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month+1, day);
        eName.setText(sharedPref.getString("name",""));

        mDates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String[] tanggal= mDates.getText().toString().split("/");
                year = Integer.parseInt(tanggal[2]);
                month = Integer.parseInt(tanggal[1])-1;
                day = Integer.parseInt(tanggal[0]);
                DatePickerDialog d = new DatePickerDialog(mContext, myDateListener, year, month, day);

//                d.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                d.show();
            }
        });

        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        rgDeliveryOption.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId==R.id.rbDeliver){
                    eName.setVisibility(View.VISIBLE);
                    rgSealOption.setVisibility(View.VISIBLE);
                    input_layout.setVisibility(View.VISIBLE);
                    linear_layout.setVisibility(View.VISIBLE);

                }else if(checkedId==R.id.rbPickStore){
                    eName.setVisibility(View.GONE);
                    rgSealOption.setVisibility(View.GONE);
                    input_layout.setVisibility(View.GONE);
                    linear_layout.setVisibility(View.GONE);
                }
            }
        });

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);

        // Spinner click listener

        // Spinner Drop down elements
        categories = new ArrayList<String>();
        MasterOngkir = new ArrayList<String>();
// Creating adapter for spinner
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        loader.setVisibility(View.VISIBLE);
        Call<Ongkir> createOrderCall = apiInterface.GetOngkirList(Integer.parseInt(sharedPref.getString("id","")));
        createOrderCall.enqueue(new Callback<Ongkir>() {
            @Override
            public void onResponse(Call<Ongkir> call, Response<Ongkir> response) {
                if(response.code()==200) {
                    Ongkir apiRes = response.body();
                    for (int i=0;i<apiRes.ongkirlist.size();i++){
                        categories.add(apiRes.ongkirlist.get(i).address);
                        MasterOngkir.add(apiRes.ongkirlist.get(i).price);
                    }
                    editor.putString("accountName",apiRes.accountName);
                    editor.putString("accountNumber",apiRes.accountNumber);
                    editor.putString("accountProvider",apiRes.accountProvider);
                    editor.apply();

                    dataAdapter.notifyDataSetChanged();
                    loader.setVisibility(View.GONE);
                    main.setVisibility(View.VISIBLE);


                }
            }

            @Override
            public void onFailure(Call<Ongkir> call, Throwable t) {
                call.cancel();
                loader.setVisibility(View.GONE);
                BuildSimpleDialog(ChooseShippingActivity.this.getString(R.string.error_title_internet), ChooseShippingActivity.this.getString(R.string.error_message_internet), "Dismiss");
            }
        });




        btnChooseDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedDeliveryOption=rgDeliveryOption.getCheckedRadioButtonId();
                if(selectedDeliveryOption==R.id.rbDeliver){
                    editor.putString("deliver_address",spinner.getSelectedItem().toString());
                    editor.putString("recipient",eName.getText().toString());
                    editor.putString("deliver_date",mDates.getText().toString());
                    int selectedSealOption=rgSealOption.getCheckedRadioButtonId();

//                Toast.makeText(getBaseContext(),String.valueOf(rgSealOption.getCheckedRadioButtonId()),Toast.LENGTH_LONG).show();
                    if(selectedSealOption==R.id.rbSealed){

                        editor.putInt("seal",1);
                    }else if(selectedSealOption==R.id.rbCount){
                        editor.putInt("seal",0);
                    }

                    Integer indexs=categories.indexOf(spinner.getSelectedItem().toString());
                    String price = MasterOngkir.get(indexs);
                    editor.putString("ongkir",price);
                    editor.putInt("order_delivery",1);
                }else if(selectedDeliveryOption==R.id.rbPickStore){

                    editor.putInt("order_delivery",2);
                }else return;
                editor.apply();
                Intent intent = new Intent(ChooseShippingActivity.this,ConfirmOrderActivity.class);
                startActivity(intent);
            }
        });
    }


    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        mDates.setText(sdf.format(calendar.getTime()));
    }
    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        new DatePickerDialog(mContext, myDateListener, year, month, day);
        Toast.makeText(mContext, "ca",
                Toast.LENGTH_SHORT)
                .show();
    }

    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(mContext, myDateListener, year, month, day);
        }
        return null;
    }

    private void showDate(int year, int month, int day) {
        mDates.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }
}
