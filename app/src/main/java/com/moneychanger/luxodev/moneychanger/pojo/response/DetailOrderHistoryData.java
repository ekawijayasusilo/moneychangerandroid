package com.moneychanger.luxodev.moneychanger.pojo.response;

import com.google.gson.annotations.SerializedName;

public class DetailOrderHistoryData {
    @SerializedName("expiredDate")
    public String expiredDate;
    @SerializedName("remainingTime")
    public String remainingTime;
    @SerializedName("accountName")
    public String accountName;
    @SerializedName("accountNumber")
    public String accountNumber;
    @SerializedName("accountProvider")
    public String accountProvider;
    @SerializedName("order")
    public OrderList.Order order;
}
