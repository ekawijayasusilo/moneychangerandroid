package com.moneychanger.luxodev.moneychanger.pojo.request;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eka wijaya susilo on 4/7/2018.
 */

public class CreateOrderData {
    @SerializedName("amount")
    public String amount;
    @SerializedName("currency_log_id")
    public String currencyLogId;
    @SerializedName("total")
    public Float total;
    @SerializedName("customer_id")
    public Integer customerId;
    @SerializedName("delivery")
    public Integer delivery;
    @Nullable
    @SerializedName("deliver_address")
    public String deliver_address;
    @Nullable
    @SerializedName("recipient")
    public String recipient;
    @Nullable
    @SerializedName("deliver_date")
    public String deliver_date;
    @Nullable
    @SerializedName("seal_type")
    public Integer seal;

    public CreateOrderData(String amount, String currencyLogId, Float total, Integer customerId, Integer delivery,String deliver_address,String recipient,String deliver_date,Integer seal){
        this.amount=amount;
        this.currencyLogId=currencyLogId;
        this.total=total;
        this.customerId=customerId;
        this.delivery=delivery;
        this.deliver_address=deliver_address;
        this.recipient=recipient;
        this.deliver_date=deliver_date;
        this.seal=seal;

    }
}
