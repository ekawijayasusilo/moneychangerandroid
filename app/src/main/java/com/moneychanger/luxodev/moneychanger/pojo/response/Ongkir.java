package com.moneychanger.luxodev.moneychanger.pojo.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Ongkir {
    @SerializedName("Ongkir")
    public List<Ongkir.Address> ongkirlist = new ArrayList();

    @SerializedName("accountName")
    public String accountName;

    @SerializedName("accountProvider")
    public String accountProvider;

    @SerializedName("accountNumber")
    public String accountNumber;



    public class Address {

        @SerializedName("id")
        public Integer id;
        @SerializedName("region_id")
        public String region_id;
        @SerializedName("address")
        public String address;
        @SerializedName("price")
        public String price;
        @SerializedName("created_at")
        public String createdAt;
        @SerializedName("updated_at")
        public String updatedAt;

    }
}
