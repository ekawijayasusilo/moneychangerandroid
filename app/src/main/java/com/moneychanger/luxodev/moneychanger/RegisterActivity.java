package com.moneychanger.luxodev.moneychanger;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.moneychanger.luxodev.moneychanger.database.DatabaseHelper;
import com.moneychanger.luxodev.moneychanger.pojo.request.RegisterPhoneOTP;
import com.moneychanger.luxodev.moneychanger.pojo.response.OTPCode;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RuntimePermissions
public class RegisterActivity extends AppCompatActivity {

    EditText etName;
    EditText etEmail;
    EditText etPassword;
    EditText etConfPassword;
    EditText etPhone;
    EditText etCardId;
    EditText etAddress;
    EditText etAddress2;
    ImageView ivKTP;
    CheckBox chk;
    Button btnRegister;

    ProgressBar spinner;
    APIInterface apiInterface;
    AlertDialog.Builder alertDialogBuilder;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    DatabaseHelper db;

    String regName;
    String regPassword;
    String regPhone;
    String regCardId;
    String regEmail;
    String regAddress;
    String regAddress2;
    String regBase64Image;
    Long regImageId;
    String imageFilePath;
    int maxSize;
    Boolean allowRegistration;

    final RegisterActivity registerActivity=this;

    private int GALLERY = 1, CAMERA = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        sharedPref=getSharedPreferences(RegisterActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        alertDialogBuilder=new AlertDialog.Builder(RegisterActivity.this);
        db = new DatabaseHelper(this);

        DisplayMetrics displayMetrics=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        maxSize=(int)(displayMetrics.widthPixels*0.8);

        allowRegistration=true;
        regImageId=sharedPref.getLong("card_image_id",0);
        regImageId=regImageId==0 ? null : regImageId;//APAKAH AMAN??
        spinner = findViewById(R.id.progressBarRegister);
        spinner.setVisibility(View.GONE);

        etName=findViewById(R.id.etName);
        etEmail=findViewById(R.id.etEmail);
        etPassword=findViewById(R.id.etPassword);
        etConfPassword=findViewById(R.id.etConfPassword);
        etCardId=findViewById(R.id.etCardId);
        etPhone=findViewById(R.id.etPhone);
        etAddress=findViewById(R.id.etAddress);
        etAddress2=findViewById(R.id.etAddress2);
        btnRegister=findViewById(R.id.btnRegister);
        ivKTP=findViewById(R.id.ivKTP);
        chk=findViewById(R.id.radio);
        //clRegister=findViewById(R.id.clRegister);


        etName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etName.setError(etName.getText().toString().trim().length() < 3 ? "Min. 3 Characters" : null);
            }
        });

        etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Pattern pattern = Patterns.EMAIL_ADDRESS;
                etEmail.setError(pattern.matcher(etEmail.getText().toString()).matches()  ? null : "Email Tidak Valid");
            }
        });

        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etPassword.setError(etPassword.getText().toString().trim().length() < 6 ? "Min. 6 Characters" : null);
            }
        });

        etConfPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etConfPassword.setError(!etConfPassword.getText().toString().equals(etPassword.getText().toString()) ? "Password doesn't match" : null);
            }
        });

        etCardId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etCardId.setError(etCardId.getText().toString().trim().length() < 16 ? "Invalid Card Id" : null);
            }
        });
        etPhone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etPhone.setError(etPhone.getText().toString().trim().length() < 8 ? "Invalid Phone Number" : null);
            }
        });

        etAddress.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etAddress.setError(etAddress.getText().toString().trim().length() < 3 ? "Min. 3 Characters" : null);
            }
        });
        etAddress2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etAddress2.setError(etAddress2.getText().toString().trim().length() < 3 ? "Min. 3 Characters" : null);
            }
        });
    }

    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void Register(View view){
        if(allowRegistration) {
            spinner.setVisibility(View.VISIBLE);
            if (!chk.isChecked()){
                spinner.setVisibility(View.GONE);
                BuildSimpleDialog("Agreement Error","You Need to Agree with Our Terms and Conditions", "Dismiss");
            }
            else if(ValidationInput()) {
                regPhone = "+62" + regPhone;
                Call<OTPCode> phoneOTPCall = apiInterface.RegisterPhoneOTP(new RegisterPhoneOTP(regPhone));
                phoneOTPCall.enqueue(new Callback<OTPCode>() {
                    @Override
                    public void onResponse(Call<OTPCode> call, Response<OTPCode> response) {
                        spinner.setVisibility(View.GONE);
                        if (response.code() == 200) {
                            OTPCode apiRes = response.body();

                            editor.putString("name", regName);
                            editor.putString("email", regEmail);
                            editor.putString("password", regPassword);
                            editor.putString("phone", regPhone);
                            editor.putString("card_id", regCardId);
                            editor.putString("address", regAddress);
                            editor.putString("address2", regAddress2);
                            editor.putString("otp_code", apiRes.otpCode);
                            editor.putLong("card_image_id", regImageId);
                            editor.putString("type", "0");

                            editor.commit();

                            Intent intentVerifyOTP = new Intent(RegisterActivity.this, VerifyOtpActivity.class);
                            startActivity(intentVerifyOTP);
                        } else if(response.code()==480){
                            BuildSimpleDialog(RegisterActivity.this.getString(R.string.error_title_unauthorized), RegisterActivity.this.getString(R.string.error_message_unauthorized), "Dismiss");
                        } else {
                            BuildSimpleDialog(RegisterActivity.this.getString(R.string.error_title_invalid_phone), RegisterActivity.this.getString(R.string.error_message_invalid_phone), "Dismiss");
                        }
                    }

                    @Override
                    public void onFailure(Call<OTPCode> call, Throwable t) {
                        call.cancel();
                        spinner.setVisibility(View.GONE);
                        BuildSimpleDialog(RegisterActivity.this.getString(R.string.error_title_internet), RegisterActivity.this.getString(R.string.error_message_internet), "Dismiss");
                    }
                });


            } else {
                spinner.setVisibility(View.GONE);
                BuildSimpleDialog(RegisterActivity.this.getString(R.string.error_title_invalid_input), RegisterActivity.this.getString(R.string.error_message_invalid_input), "Dismiss");
            }
        }
    }

    public boolean ValidationInput(){
        boolean validationResult=true;

        regName=etName.getText().toString();
        regEmail=etEmail.getText().toString();
        regPassword=etPassword.getText().toString();
        regPhone=etPhone.getText().toString();
        regCardId=etCardId.getText().toString();
        regAddress=etAddress.getText().toString();
        regAddress2=etAddress2.getText().toString();

        Pattern pattern = Patterns.EMAIL_ADDRESS;
        validationResult= pattern.matcher(etEmail.getText().toString()).matches() && validationResult;
        validationResult=regName.length()>=3 && validationResult;
        validationResult=regPassword.equals(etConfPassword.getText().toString()) && regPassword.length()>=6 && validationResult;
        validationResult=regPhone.length()>=8 && validationResult;
        validationResult=regCardId.length()==16 && validationResult;
        validationResult=regAddress.length()>=3 && validationResult;
        validationResult=regBase64Image!=null && validationResult;

        return validationResult;
    }

    public void PickKTPImage(View view) {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {"Select photo from gallery", "Capture photo from camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                RegisterActivityPermissionsDispatcher.choosePhotoFromGalleryWithPermissionCheck(registerActivity);
                                break;
                            case 1:
                                RegisterActivityPermissionsDispatcher.takePhotoFromCameraWithPermissionCheck(registerActivity);
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    public void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    public void takePhotoFromCamera() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(pictureIntent.resolveActivity(getPackageManager()) != null){
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
                return;
            }
            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(this, getPackageName() +".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(pictureIntent, CAMERA);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        RegisterActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
    void showRationaleForReadExternal(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_read_external_rationale)
                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.READ_EXTERNAL_STORAGE)
    void showDeniedForReadExternal() {
        Toast.makeText(this, R.string.permission_read_external_denied, Toast.LENGTH_SHORT).show();
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showRationaleForCamera(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_camera_rationale)
                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showDeniedForCamera() {
        Toast.makeText(this, R.string.permission_camera_denied, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    Bitmap modifiedBM=TransformBitmap(bitmap);
                    regBase64Image = BitmapToBase64(modifiedBM);

                    if(regImageId==null) regImageId=db.insertKTPImage(regBase64Image);
                    else db.updateKTPImage(regImageId,regBase64Image);

                    ivKTP.setImageBitmap(modifiedBM);
                    Toast.makeText(RegisterActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(RegisterActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            new AsyncTaskRunner().execute(imageFilePath);
        }
    }

    public String BitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public Bitmap TransformBitmap(Bitmap bm){
        int outWidth;
        int outHeight;
        int inWidth = bm.getWidth();
        int inHeight = bm.getHeight();
        if(inWidth > inHeight){
            outWidth = maxSize;
            outHeight = (inHeight * maxSize) / inWidth;
        } else {
            outHeight = maxSize;
            outWidth = (inWidth * maxSize) / inHeight;
        }
        Matrix matrix = new Matrix();
        matrix.postRotate(90);

        Bitmap modifiedBM=Bitmap.createScaledBitmap(bm, outWidth, outHeight, false);
        if(inWidth<inHeight){
            modifiedBM=Bitmap.createBitmap(modifiedBM , 0, 0, modifiedBM .getWidth(), modifiedBM .getHeight(), matrix, true);
        }
        return modifiedBM;
    }

    private File createImageFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();
        return image;
    }

    public class AsyncTaskRunner extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(RegisterActivity.this.getContentResolver(), Uri.parse("file://" + imageFilePath));
                Bitmap modifiedBM=TransformBitmap(bitmap);
                regBase64Image = BitmapToBase64(modifiedBM);

                if(regImageId==null) regImageId=db.insertKTPImage(regBase64Image);
                else db.updateKTPImage(regImageId,regBase64Image);
                return modifiedBM;
            } catch (IOException e) {
                e.printStackTrace();
                return bitmap;
            }
        }


        @Override
        protected void onPostExecute(Bitmap modifiedBM) {
            allowRegistration=true;
            if(modifiedBM==null){
                Toast.makeText(RegisterActivity.this, "Failed to Take Image!", Toast.LENGTH_SHORT).show();
            }else{
                ivKTP.setImageBitmap(modifiedBM);
                Toast.makeText(RegisterActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
            }
        }


        @Override
        protected void onPreExecute() {
            allowRegistration=false;
        }
    }

}
