package com.moneychanger.luxodev.moneychanger;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.moneychanger.luxodev.moneychanger.database.DatabaseHelper;
import com.moneychanger.luxodev.moneychanger.pojo.response.RegisteredCustomer;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileFragment extends Fragment {

    APIInterface apiInterface;
    AlertDialog.Builder alertDialogBuilder;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    DatabaseHelper db;

//    String base64Image;
//    String imageHash;


    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPref=getActivity().getSharedPreferences(getActivity().getString(R.string.app_name), Context.MODE_PRIVATE);
        editor=sharedPref.edit();
        alertDialogBuilder=new AlertDialog.Builder(getActivity());
        apiInterface = APIClient.getClient().create(APIInterface.class);
        db = new DatabaseHelper(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        String base64Image=db.getKTPImage(sharedPref.getLong("card_image_id",0)); //APAKAH AMAN????
        String imageHash=md5(base64Image);
        SetProfile(Base64ToBitmap(base64Image));
        RequestNewProfile(imageHash);
    }

    public Bitmap Base64ToBitmap(String image64){
        byte[] decodedString = Base64.decode(image64, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    public void SetProfile(Bitmap bm){
        try{
            ((TextView) getView().findViewById(R.id.tvProfileName)).setText(sharedPref.getString("name",null));
            ((TextView) getView().findViewById(R.id.tvProfileCardID)).setText(sharedPref.getString("card_id",null));
            ((TextView) getView().findViewById(R.id.tvProfilePhone)).setText(sharedPref.getString("phone",null));
            ((TextView) getView().findViewById(R.id.tvProfileAddress)).setText(sharedPref.getString("address",null));
            ((TextView) getView().findViewById(R.id.tvProfileAddress2)).setText(sharedPref.getString("address2",null));
            ((TextView) getView().findViewById(R.id.tvProfileEmail)).setText(sharedPref.getString("email",null));
            if(bm!=null) ((ImageView) getView().findViewById(R.id.ivProfileKTP)).setImageBitmap(bm);
        }
        catch (Exception e){

        }

    }

    public void RequestNewProfile(String imageHash){
        Call<RegisteredCustomer> profileCall = apiInterface.GetProfileData(sharedPref.getString("id",null), imageHash);
        profileCall.enqueue(new Callback<RegisteredCustomer>() {
            @Override
            public void onResponse(Call<RegisteredCustomer> call, Response<RegisteredCustomer> response) {
                if(response.code()==274 || response.code()==200){
                    RegisteredCustomer apiRes=response.body();
                    //if(apiRes==null) Toast.makeText(getActivity(),"null apires",Toast.LENGTH_LONG).show();
                    WritePref(apiRes.customer);

                    if(response.code()==200){
                        //Toast.makeText(getActivity(),"rewrite db",Toast.LENGTH_LONG).show();
                        db.updateKTPImage(sharedPref.getLong("card_image_id",0),apiRes.customer.cardImage);
                        SetProfile(Base64ToBitmap(apiRes.customer.cardImage));
                        return;
                    }
                    SetProfile(null);
                }else if(response.code()==480){
                    BuildSimpleDialog(getActivity().getString(R.string.error_title_unauthorized), getActivity().getString(R.string.error_message_unauthorized), "Dismiss");
                }else{
                    BuildSimpleDialog(getActivity().getString(R.string.error_title_server_error), getActivity().getString(R.string.error_message_server_error), "Dismiss");
                }
            }

            @Override
            public void onFailure(Call<RegisteredCustomer> call, Throwable t) {
                call.cancel();
                BuildSimpleDialog(getActivity().getString(R.string.error_title_internet), getActivity().getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }

    public void WritePref(RegisteredCustomer.Customer customer){
        editor.putString("name", customer.name);
        editor.putString("card_id", customer.cardId);
        editor.putString("phone", customer.phone);
        editor.putString("address", customer.address);
        editor.commit();
    }

    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public String md5(String input) {
        try {
            String result = input;
            if(input != null) {
                MessageDigest md = MessageDigest.getInstance("MD5"); //or "SHA-1"
                md.update(input.getBytes());
                BigInteger hash = new BigInteger(1, md.digest());
                result = hash.toString(16);
                while(result.length() < 32) { //40 for SHA-1
                    result = "0" + result;
                }
            }
            return result;
        }catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
