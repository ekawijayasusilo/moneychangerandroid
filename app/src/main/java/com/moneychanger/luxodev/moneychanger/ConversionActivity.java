package com.moneychanger.luxodev.moneychanger;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.moneychanger.luxodev.moneychanger.pojo.response.CurrencyList;
import com.moneychanger.luxodev.moneychanger.pojo.response.DetailCurrencyList;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConversionActivity extends AppCompatActivity {

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    APIInterface apiInterface;
    DetailCurrencyList mData;
    List<EditText> etAmountList;
    TextInputLayout til;
    ArrayList<String> currency;
    ArrayList<Integer> amount;
    ArrayAdapter<String> dataAdapter;
    ArrayList<DetailCurrencyList> Detail;
    EditText etamount;
    TextView tvresult;
    TabLayout tab;
    Locale localeID;
    NumberFormat numberFormat;
    AlertDialog.Builder alertDialogBuilder;
    Float orderTotal;
    Float maximumOrder;
    SwipeRefreshLayout swipeRefresh;

    ProgressBar loader;
    CardView cardClosedShop;
    RecyclerView rvCollapsibleCurrency;
    ConversionActivity.ConfirmDetailOrderAdapter collapsibleCurrencyAdapter;
    Gson gson;

    ConstraintLayout main;

    int new_tot;
    Spinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.mcPrimary), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        setTitle("Konversi");
        localeID = new Locale("in", "ID");
        alertDialogBuilder=new AlertDialog.Builder(ConversionActivity.this);
        orderTotal=0f;
        maximumOrder=0f;
        sharedPref = getSharedPreferences(ConversionActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        gson = new Gson();
        main = (ConstraintLayout) findViewById(R.id.mainLayout);
        numberFormat = NumberFormat.getCurrencyInstance(localeID);
        etAmountList=new ArrayList<>();
        amount=new ArrayList<Integer>();
        loader = findViewById(R.id.progressBarCurrency);
//        spinner.setVisibility(View.GONE);
        cardClosedShop=findViewById(R.id.cardClosedShop);
        cardClosedShop.setVisibility(View.GONE);
        main.setVisibility(View.GONE);
        rvCollapsibleCurrency=findViewById(R.id.rvCurrency);
        collapsibleCurrencyAdapter = new ConversionActivity.ConfirmDetailOrderAdapter(new ArrayList<DetailCurrencyList.DetailCurrency>(), new ArrayList<Integer>());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvCollapsibleCurrency.setLayoutManager(mLayoutManager);
        rvCollapsibleCurrency.setItemAnimator(new DefaultItemAnimator());
        rvCollapsibleCurrency.setAdapter(collapsibleCurrencyAdapter);
        til=(TextInputLayout) findViewById(R.id.til);
        tab=(TabLayout) findViewById(R.id.tabLayout);
        final TextView tvcurrency =(TextView) findViewById(R.id.tvcurrency);
        tvresult =(TextView) findViewById(R.id.tvresult);
        etamount =(EditText) findViewById(R.id.etAmount);
        Detail=new ArrayList<DetailCurrencyList>();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        sharedPref = getSharedPreferences(ConversionActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
        RequestDetailCurrencyList(false);
         spinner = (Spinner) findViewById(R.id.smoney);

        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        RequestDetailCurrencyList(true);
                    }
                }
        );
        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                etamount.setText("");
                tvresult.setText("");

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        currency=new ArrayList<String>();
        dataAdapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_spinner_item, currency);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        ArrayList<ArrayList<String>> result= new ArrayList<ArrayList<String>>();

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    Toast.makeText(getBaseContext(),position,Toast.LENGTH_LONG).show();
                    collapsibleCurrencyAdapter.updatecurrency(Detail.get(position).detailCurrencyList);
                    etAmountList = Arrays.asList(new EditText[Detail.get(position).detailCurrencyList.size()]);
//                    amount = (ArrayList<Integer>) Arrays.asList(new Integer[mData.detailCurrencyList.size()]);

//                    collapsibleCurrencyAdapter.notifyDataSetChanged();

                    available();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            etamount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    available();
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void RequestDetailCurrencyList(final boolean isRefresh){
        if(!isRefresh) loader.setVisibility(View.VISIBLE);

        Call<DetailCurrencyList> detailCurrencyCall = apiInterface.GetDetailCurrencyList(sharedPref.getString("id",null));
        detailCurrencyCall.enqueue(new Callback<DetailCurrencyList>() {
            @Override
            public void onResponse(Call<DetailCurrencyList> call, Response<DetailCurrencyList> response) {
                if(response.code()==200) {

                    DetailCurrencyList apiRes = response.body();
                    if(apiRes.currencyList.size()>0) {
                        maximumOrder=apiRes.maximumOrder;
                        mData = apiRes;
                        for (CurrencyList.Currency c : mData.currencyList) {
                            currency.add(c.name + " - " + c.code);
                            ArrayList<DetailCurrencyList.DetailCurrency> dets = new ArrayList<DetailCurrencyList.DetailCurrency>();
                            for (int i = 0; i < mData.detailCurrencyList.size(); i++) {
                                if (mData.detailCurrencyList.get(i).currencyId.equals(c.id)) {
                                    mData.detailCurrencyList.get(i).codeCur = c.code;
                                    dets.add(mData.detailCurrencyList.get(i));
//                                Toast.makeText(getApplicationContext(),mData.detailCurrencyList.get(i).codeCur+"("+mData.detailCurrencyList.get(i).stock+"): "+mData.detailCurrencyList.get(i).price,Toast.LENGTH_LONG).show();

                                }
                            }
                            DetailCurrencyList temp = new DetailCurrencyList();
                            temp.detailCurrencyList = dets;
                            Detail.add(temp);

                        }
                        Log.w("wow", "detail: "+ Detail.get(0).detailCurrencyList);
                        Log.w("wow", "apiRes: "+ apiRes.detailCurrencyList.get(0));

                        dataAdapter.notifyDataSetChanged();
                        loader.setVisibility(View.GONE);
                        cardClosedShop.setVisibility(View.GONE);
                        main.setVisibility(View.VISIBLE);
                        if(isRefresh)swipeRefresh.setRefreshing(false);

                    }
                    else{
                        if(isRefresh)swipeRefresh.setRefreshing(false);
                        cardClosedShop.setVisibility(View.VISIBLE);

                        loader.setVisibility(View.GONE);
                    }

                }

            }

            @Override
            public void onFailure(Call<DetailCurrencyList> call, Throwable t) {

            }
    });
    }

    public int PickNominal (Integer total, ArrayList<DetailCurrencyList.DetailCurrency> nominal, int max){
        Log.w("wow", "total: "+total+" max"+max);

        if(total==0)
            return max;

        for(int i=0;i<nominal.size();i++){
            Log.w("wow", "nom: "+nominal.get(i).nominal+"stk"+nominal.get(i).stock);

            if(total-nominal.get(i).nominal.intValue() >=0 && nominal.get(i).stock>0){
                ArrayList<DetailCurrencyList.DetailCurrency> new_nominal= new ArrayList<DetailCurrencyList.DetailCurrency>(nominal.size());

//                new_nominal =(ArrayList<DetailCurrencyList.DetailCurrency>) nominal.clone();
                for (DetailCurrencyList.DetailCurrency item : nominal) new_nominal.add(new DetailCurrencyList.DetailCurrency(item));
                new_tot= total-nominal.get(i).nominal.intValue();
                Log.w("wow", "total: "+total+" nom"+new_tot+"max"+max);
                new_nominal.get(i).stock=new_nominal.get(i).stock-1;
                amount.set(i,amount.get(i)+1);

                return PickNominal(new_tot,  new_nominal,max+nominal.get(i).nominal.intValue());
            }
        }
        return max;
    }

    public void available(){
        if (!etamount.getText().toString().equals("")) {

            int pos = spinner.getSelectedItemPosition();
            amount=new ArrayList<>();
            amount.clear();
            for(int i=0;i<Detail.get(pos).detailCurrencyList.size();i++){
                amount.add(0);
            }
            Double mutator;
            if(tab.getSelectedTabPosition()==0) {
//            Toast.makeText()
                mutator=Double.parseDouble(etamount.getText().toString());
//            mods=mutator;
                Log.w("wow", "Coke: " + mutator);

            }
            else {
                int amount = Integer.parseInt(etamount.getText().toString());
                ArrayList<String> temp = new ArrayList<String>();
                mutator = Math.ceil(amount / Detail.get(pos).detailCurrencyList.get(0).price);
                Log.w("wow", "Coke: " + mutator);
            }

            Log.w("wow", "available: "+ Detail.get(pos).detailCurrencyList);
            Log.w("wow", "et: "+etamount.getText().toString());
            int multip=PickNominal(mutator.intValue(), (ArrayList<DetailCurrencyList.DetailCurrency>) Detail.get(pos).detailCurrencyList,0);
            int mon =Detail.get(pos).detailCurrencyList.get(0).price.intValue();
            Log.w("wow", "mul: "+multip);
            Log.w("wow", "mon: "+mon);
            if(multip<=0){
                til.setError("Nominal tidak tersedia Untuk jumlah Yang diharapkan");
            }
            else{
                til.setError("");

            }
            int res =mon * multip;
            Log.w("wow", "res: "+res);

            tvresult.setText(multip + " " + Detail.get(pos).detailCurrencyList.get(0).codeCur + " x " + numberFormat.format(mon) + " = " + numberFormat.format(res));
            Log.w("wow", "amount: "+amount);
            collapsibleCurrencyAdapter.updateamount(amount);
            collapsibleCurrencyAdapter.notifyDataSetChanged();

        }
        else{
            tvresult.setText("-");

        }

    }

    public final void ProceedOrder(View view){
        if(orderTotal>0f){
            if(orderTotal<=maximumOrder) {
                StringBuilder sbOrderAmountList = new StringBuilder();
                for (EditText et : etAmountList) {
                    if (et!=null && !TextUtils.isEmpty(et.getText().toString())) {
                        sbOrderAmountList.append(et.getText().toString()).append(",");
                    } else {
                        sbOrderAmountList.append("0").append(",");
                    }

                }
                int pos = spinner.getSelectedItemPosition();

                String jsonDetailCurrencyList = gson.toJson(Detail.get(pos).detailCurrencyList);

                editor.putString("detail_currency_list", jsonDetailCurrencyList);
                editor.putString("order_amount_list", sbOrderAmountList.toString());
                editor.putFloat("order_total", orderTotal);
                editor.commit();

                Intent intent = new Intent(ConversionActivity.this, ChooseShippingActivity.class);
                startActivity(intent);
            }else{
                BuildSimpleDialog(ConversionActivity.this.getString(R.string.error_title_exceed_maximum), ConversionActivity.this.getString(R.string.error_message_exceed_maximum)+"("+numberFormat.format(maximumOrder)+").", "Dismiss");
            }
        }else{
            BuildSimpleDialog(ConversionActivity.this.getString(R.string.error_title_null_order), ConversionActivity.this.getString(R.string.error_message_null_order), "Ok");
        }
    }

    public class ConfirmDetailOrderAdapter extends RecyclerView.Adapter<ConversionActivity.ConfirmDetailOrderAdapter.ConfirmDetailOrderViewHolder> {

        private List<DetailCurrencyList.DetailCurrency> adapterConfirmDetailOrderList;
        private ArrayList<Integer> adapterOrderAmountList;

        public class ConfirmDetailOrderViewHolder extends RecyclerView.ViewHolder {
            public TextView tvDCValue, tvDCPrice;
            public EditText etDCAmount;

            public ConfirmDetailOrderViewHolder(View view) {
                super(view);
                tvDCValue = view.findViewById(R.id.tvDCValue);
                tvDCPrice = view.findViewById(R.id.tvDCPrice);
                etDCAmount = view.findViewById(R.id.etDCAmount);
            }
        }

        public ConfirmDetailOrderAdapter(List<DetailCurrencyList.DetailCurrency> adapterConfirmDetailOrderList, ArrayList<Integer> adapterOrderAmountList) {
            this.adapterConfirmDetailOrderList = adapterConfirmDetailOrderList;
            this.adapterOrderAmountList=adapterOrderAmountList;
        }

        public void updatecurrency(List<DetailCurrencyList.DetailCurrency> Curren){
            this.adapterConfirmDetailOrderList=Curren;

        }

        public void updateamount(ArrayList<Integer> Curren){
            this.adapterOrderAmountList=Curren;


        }

        @Override
        public ConversionActivity.ConfirmDetailOrderAdapter.ConfirmDetailOrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_detailcurrency, parent, false);

            return new ConversionActivity.ConfirmDetailOrderAdapter.ConfirmDetailOrderViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final ConversionActivity.ConfirmDetailOrderAdapter.ConfirmDetailOrderViewHolder holder, final int position) {
            try{
                int pos = spinner.getSelectedItemPosition();

                for(int i=0; i< adapterConfirmDetailOrderList.size(); i++){
                    if(Detail.get(pos).detailCurrencyList.get(i).id==adapterConfirmDetailOrderList.get(position).id){
                        etAmountList.set(i,holder.etDCAmount);
                    }
                }
                holder.tvDCValue.setText(adapterConfirmDetailOrderList.get(position).codeCur + Integer.toString(adapterConfirmDetailOrderList.get(position).nominal));
                holder.tvDCPrice.setText(numberFormat.format(adapterConfirmDetailOrderList.get(position).price));

                holder.etDCAmount.setText(String.valueOf(this.adapterOrderAmountList.get(position)));
                holder.etDCAmount.addTextChangedListener(new TextWatcher() {
                    public void afterTextChanged(Editable s) {
                        if (!TextUtils.isEmpty(holder.etDCAmount.getText().toString()) && Integer.parseInt(holder.etDCAmount.getText().toString()) > adapterConfirmDetailOrderList.get(position).stock) {
                            holder.etDCAmount.setText("");
                            BuildSimpleDialog(ConversionActivity.this.getString(R.string.error_title_insufficient_stock_2), ConversionActivity.this.getString(R.string.error_message_insufficient_stock_2), "Dismiss");
                        }
                        CalculateTotalOrder();
                    }
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                });
                Log.w("wow", "onBindViewHolder: ");

            }
            catch (Exception e){
//                BuildSimpleDialog(ConversionActivity.this.getString(R.string.error_title_insufficient_stock_2), ConversionActivity.this.getString(R.string.error_message_insufficient_stock_2), "Dismiss");

            }


        }

        @Override
        public int getItemCount() {
            return adapterConfirmDetailOrderList.size();
        }
    }

    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public final void CalculateTotalOrder(){
        orderTotal=0f;
        int pos = spinner.getSelectedItemPosition();

        for(int i=0;i<etAmountList.size();i++){

            if(etAmountList.get(i)!=null) {
                if (!TextUtils.isEmpty(etAmountList.get(i).getText().toString())) {
                    Log.w("wow","et amoutn list"+etAmountList.get(i).getText().toString());
                    Log.w("wow","price"+Detail.get(pos).detailCurrencyList.get(i).price);
                    Log.w("wow","nominal"+Detail.get(pos).detailCurrencyList.get(i).nominal);

                    orderTotal += (Integer.parseInt(etAmountList.get(i).getText().toString()) * Detail.get(pos).detailCurrencyList.get(i).price * Detail.get(pos).detailCurrencyList.get(i).nominal);
                }
            }
        }
        if(orderTotal>maximumOrder){
            BuildSimpleDialog(ConversionActivity.this.getString(R.string.error_title_exceed_maximum), ConversionActivity.this.getString(R.string.error_message_exceed_maximum)+"("+numberFormat.format(maximumOrder)+").", "Dismiss");
        }
        Log.w("wow","astaga kcalculate ulang "+orderTotal);
        Log.w("wow","astaga kcalculate ulang "+orderTotal);
//        tvOrderTotal.setText(numberFormat.format(orderTotal));
    }
}
