package com.moneychanger.luxodev.moneychanger;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_Launcher);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        SharedPreferences sharedPref=getSharedPreferences(SplashActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor=sharedPref.edit();
//        editor.clear();
//        editor.commit();

        EvaluateAppState();
    }

    public void EvaluateAppState(){

        SharedPreferences sharedPref=getSharedPreferences(SplashActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);

        // No SP data => Register
        if(sharedPref.getString("id",null)==null){
            //setTheme(R.style.AppTheme);
            Intent intentRegister = new Intent(SplashActivity.this,FullscreenActivity.class);
            intentRegister.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intentRegister);
            return;
        }

        // Unapproved => Wait for Approval
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<Void> approvalCall = apiInterface.GetUserApproval(sharedPref.getString("id",null));
        approvalCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code()==461){
                    //setTheme(R.style.AppTheme);
                    Intent intentWaitApproval = new Intent(SplashActivity.this,WaitApprovalActivity.class);
                    intentWaitApproval.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentWaitApproval);
                }else if(response.code()==462){
                    SharedPreferences sharedPref=getSharedPreferences(SplashActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("id",null);
                    editor.putString("name",null);
                    editor.putString("password",null);
                    editor.putString("phone",null);
                    editor.putString("card_id",null);
                    editor.putString("address",null);
                    editor.putString("otp_code",null);
                    editor.apply();

                    //setTheme(R.style.AppTheme);
                    Intent intentRegister = new Intent(SplashActivity.this,FullscreenActivity.class);
                    intentRegister.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentRegister);
                }else if(response.code()==200){
                    //setTheme(R.style.AppTheme);
                    Intent intentLogin = new Intent(SplashActivity.this,LoginActivity.class);
                    intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentLogin);
                }else{
                    AlertDialog alertDialog = new AlertDialog.Builder(SplashActivity.this).create();
                    alertDialog.setTitle(R.string.error_title_server_error);
                    alertDialog.setMessage(SplashActivity.this.getString(R.string.error_message_server_error));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Close",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    finish();
                                    System.exit(0);
                                }
                            });
                    alertDialog.show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                call.cancel();

                AlertDialog alertDialog = new AlertDialog.Builder(SplashActivity.this).create();
                alertDialog.setTitle(R.string.error_title_internet);
                alertDialog.setMessage(SplashActivity.this.getString(R.string.error_message_internet));
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Close",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                                System.exit(0);
                            }
                        });
                alertDialog.show();
            }
        });
    }
}
