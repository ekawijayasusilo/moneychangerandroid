package com.moneychanger.luxodev.moneychanger;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WaitApprovalActivity extends AppCompatActivity {

    SharedPreferences sharedPref;
    APIInterface apiInterface;
    AlertDialog.Builder alertDialogBuilder;
    SwipeRefreshLayout swipeRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wait_approval);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        sharedPref=getSharedPreferences(WaitApprovalActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
        alertDialogBuilder=new AlertDialog.Builder(WaitApprovalActivity.this);

        swipeRefresh= findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        requestApprovalStatus(true);
                    }
                }
        );

        requestApprovalStatus(false);
    }

    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void requestApprovalStatus(final boolean isRefresh){
        Call<Void> approvalCall = apiInterface.GetUserApproval(sharedPref.getString("id",null));
        approvalCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(isRefresh)swipeRefresh.setRefreshing(false);
                if(response.code()==200){
                    Intent intentLogin = new Intent(WaitApprovalActivity.this,LoginActivity.class);
                    intentLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentLogin);
                }else if(response.code()==462){
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.remove("id");
                    editor.remove("name");
                    editor.remove("password");
                    editor.remove("phone");
                    editor.remove("card_id");
                    editor.remove("address");
                    editor.remove("otp_code");
                    editor.apply();

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.setTitle(R.string.error_title_rejected_account);
                    alertDialog.setMessage(WaitApprovalActivity.this.getString(R.string.error_message_rejected_account));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Dismiss",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    Intent intentRegister = new Intent(WaitApprovalActivity.this,RegisterActivity.class);
                                    intentRegister.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intentRegister);
                                }
                            });
                    alertDialog.show();
                }else if(response.code()==480){
                    BuildSimpleDialog(WaitApprovalActivity.this.getString(R.string.error_title_unauthorized), WaitApprovalActivity.this.getString(R.string.error_message_unauthorized), "Dismiss");
                }else if(response.code()!=461){
                    BuildSimpleDialog(WaitApprovalActivity.this.getString(R.string.error_title_server_error), WaitApprovalActivity.this.getString(R.string.error_message_server_error), "Dismiss");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                call.cancel();
                if(isRefresh)swipeRefresh.setRefreshing(false);
                BuildSimpleDialog(WaitApprovalActivity.this.getString(R.string.error_title_internet), WaitApprovalActivity.this.getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }
}
