package com.moneychanger.luxodev.moneychanger.pojo.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eka wijaya susilo on 3/30/2018.
 */

public class RegisterCustomer {
    @SerializedName("name")
    public String name;
    @SerializedName("imei")
    public String imei;
    @SerializedName("card_id")
    public String cardId;
    @SerializedName("phone")
    public String phone;
    @SerializedName("card_image")
    public String cardImage;
    @SerializedName("address")
    public String address;
    @SerializedName("address2")
    public String address2;
    @SerializedName("email")
    public String email;


    public RegisterCustomer(String name, String imei, String cardId, String phone, String cardImage, String address,String address2,String email){
        this.name=name;
        this.imei=imei;
        this.cardId=cardId;
        this.phone=phone;
        this.cardImage=cardImage;
        this.address=address;
        this.address2=address2;
        this.email=email;
    }
}
