package com.moneychanger.luxodev.moneychanger;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.moneychanger.luxodev.moneychanger.pojo.response.CurrencyList;
import com.moneychanger.luxodev.moneychanger.pojo.response.DetailCurrencyList;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.listeners.GroupExpandCollapseListener;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailCurrencyActivity extends AppCompatActivity {

    List<DetailCurrencyList.DetailCurrency> detailCurrencyList;
    CollapsibleCurrencyAdapter collapsibleCurrencyAdapter;
    List<EditText> etAmountList;

    RecyclerView rvCollapsibleCurrency;
    SwipeRefreshLayout swipeRefresh;
    ProgressBar spinner;
    CardView cardClosedShop;
    Button btnProceedOrder;
    TextView tvOrderTotal;
    TextView textView17;
    TextView textBuy;

    Float orderTotal;
    Float maximumOrder;

    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    APIInterface apiInterface;
    AlertDialog.Builder alertDialogBuilder;
    Locale localeID;
    NumberFormat numberFormat;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_currency);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        localeID = new Locale("in", "ID");
        numberFormat = NumberFormat.getCurrencyInstance(localeID);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        sharedPref = getSharedPreferences(DetailCurrencyActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
        editor = sharedPref.edit();
        alertDialogBuilder=new AlertDialog.Builder(DetailCurrencyActivity.this);
        gson = new Gson();

        detailCurrencyList=new ArrayList<>();
        etAmountList=new ArrayList<>();
        orderTotal=0f;
        maximumOrder=0f;

        tvOrderTotal = findViewById(R.id.tvDCTotal);
        tvOrderTotal.setText(numberFormat.format(orderTotal));
        textView17=findViewById(R.id.textView17);
        btnProceedOrder=findViewById(R.id.btnProceedOrder);
        cardClosedShop=findViewById(R.id.cardClosedShop2);
        cardClosedShop.setVisibility(View.VISIBLE);
        btnProceedOrder.setVisibility(View.GONE);
        tvOrderTotal.setVisibility(View.GONE);
        textView17.setVisibility(View.GONE);

        rvCollapsibleCurrency=findViewById(R.id.rvCollapsibleCurrency);
        collapsibleCurrencyAdapter = new CollapsibleCurrencyAdapter(new ArrayList<CollapsibleCurrency>());
//        collapsibleCurrencyAdapter.setOnGroupExpandCollapseListener(new GroupExpandCollapseListener() {
//            @Override
//            public void onGroupExpanded(ExpandableGroup group) {}
//
//            @Override
//            public void onGroupCollapsed(ExpandableGroup group) {
//                for(int i=0; i<group.getItems().size(); i++){
//                    for(int j=0; j<detailCurrencyList.size(); j++){
//                        if(detailCurrencyList.get(j).id==group.getItems().get(i)){
//                            etAmountList.get(j).setText("");
//                            etAmountList.set(j,null);
//                            break;
//                        }
//                    }
//                }
//            }
//        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvCollapsibleCurrency.setLayoutManager(mLayoutManager);
        rvCollapsibleCurrency.setItemAnimator(new DefaultItemAnimator());
        rvCollapsibleCurrency.setAdapter(collapsibleCurrencyAdapter);

        spinner = findViewById(R.id.progressBarDetailCurrency);
        spinner.setVisibility(View.GONE);

        swipeRefresh=findViewById(R.id.swipeRefreshDetailCurrency);
        swipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        RequestDetailCurrencyList(true);
                    }
                }
        );

        RequestDetailCurrencyList(false);

    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void RequestDetailCurrencyList(final boolean isRefresh){
        if(!isRefresh) spinner.setVisibility(View.VISIBLE);
        Call<DetailCurrencyList> detailCurrencyCall = apiInterface.GetDetailCurrencyList(sharedPref.getString("id",null));
        detailCurrencyCall.enqueue(new Callback<DetailCurrencyList>() {
            @Override
            public void onResponse(Call<DetailCurrencyList> call, Response<DetailCurrencyList> response) {
                if(response.code()==200) {

                    DetailCurrencyList apiRes = response.body();
                    collapsibleCurrencyAdapter = new CollapsibleCurrencyAdapter(ConstructCollapsibleCurrencyList(apiRes.currencyList,apiRes.detailCurrencyList));
                    collapsibleCurrencyAdapter.setOnGroupExpandCollapseListener(new GroupExpandCollapseListener() {
                        @Override
                        public void onGroupExpanded(ExpandableGroup group) {}

                        @Override
                        public void onGroupCollapsed(ExpandableGroup group) {
                            for(int i=0; i<group.getItems().size(); i++){
                                for(int j=0; j<detailCurrencyList.size(); j++){
                                    if(detailCurrencyList.get(j).id==((CollapsibleCurrency.ContentDetailCurrency) group.getItems().get(i)).id){
                                        etAmountList.get(j).setText("");
                                        etAmountList.set(j,new EditText(DetailCurrencyActivity.this));
                                        etAmountList.set(j,null);
                                        break;
                                    }
                                }
                            }
                            Log.w("wow","astaga collapse");
                            CalculateTotalOrder();
                        }
                    });
                    rvCollapsibleCurrency.setAdapter(collapsibleCurrencyAdapter);
                    CalculateTotalOrder();

                    maximumOrder=apiRes.maximumOrder;
                    if(apiRes.currencyList.size()==0){
                        cardClosedShop.setVisibility(View.VISIBLE);
                        btnProceedOrder.setVisibility(View.GONE);
                        tvOrderTotal.setVisibility(View.GONE);
                        textView17.setVisibility(View.GONE);
                    }
                    else{
                        cardClosedShop.setVisibility(View.GONE);
                        btnProceedOrder.setVisibility(View.VISIBLE);
                        tvOrderTotal.setVisibility(View.VISIBLE);
                        textView17.setVisibility(View.VISIBLE);
                    }

                }else if(response.code()==480){
                    BuildSimpleDialog(DetailCurrencyActivity.this.getString(R.string.error_title_unauthorized), DetailCurrencyActivity.this.getString(R.string.error_message_unauthorized), "Dismiss");
                }else{
                    BuildSimpleDialog(DetailCurrencyActivity.this.getString(R.string.error_title_server_error), DetailCurrencyActivity.this.getString(R.string.error_message_server_error), "Dismiss");
                }
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<DetailCurrencyList> call, Throwable t) {
                call.cancel();
                if(isRefresh)swipeRefresh.setRefreshing(false);
                else spinner.setVisibility(View.GONE);
                BuildSimpleDialog(DetailCurrencyActivity.this.getString(R.string.error_title_internet), DetailCurrencyActivity.this.getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }

    public final void CalculateTotalOrder(){
        orderTotal=0f;
        for(int i=0;i<etAmountList.size();i++){
            if(etAmountList.get(i)!=null) {
                if (!TextUtils.isEmpty(etAmountList.get(i).getText().toString())) {
                    Log.w("wow","et amoutn list"+etAmountList.get(i).getText().toString());
                    Log.w("wow","price"+detailCurrencyList.get(i).price);
                    Log.w("wow","nominal"+detailCurrencyList.get(i).nominal);

                    orderTotal += (Integer.parseInt(etAmountList.get(i).getText().toString()) * detailCurrencyList.get(i).price * detailCurrencyList.get(i).nominal);
                }
            }
        }
        if(orderTotal>maximumOrder){
            BuildSimpleDialog(DetailCurrencyActivity.this.getString(R.string.error_title_exceed_maximum), DetailCurrencyActivity.this.getString(R.string.error_message_exceed_maximum)+"("+numberFormat.format(maximumOrder)+").", "Dismiss");
        }
        Log.w("wow","astaga calculate ulang");
        tvOrderTotal.setText(numberFormat.format(orderTotal));
    }

    public List<CollapsibleCurrency> ConstructCollapsibleCurrencyList(List<CurrencyList.Currency> cl, List<DetailCurrencyList.DetailCurrency> dcl){
        ArrayList<CollapsibleCurrency> ccl=new ArrayList<>();
        detailCurrencyList.clear();
        CollapsibleCurrency ccStatic=new CollapsibleCurrency();
        for(CurrencyList.Currency c : cl){
            ArrayList<CollapsibleCurrency.ContentDetailCurrency> cdcl=new ArrayList<>();
            for(int i=0;i<dcl.size();i++){
                if(dcl.get(i).currencyId.equals(c.id)){
                    cdcl.add(ccStatic.new ContentDetailCurrency(dcl.get(i)));
                    dcl.get(i).codeCur=c.code;
                    detailCurrencyList.add(dcl.get(i));
                }
            }
            ccl.add(new CollapsibleCurrency(c.code,cdcl,c));
        }
        etAmountList = Arrays.asList(new EditText[detailCurrencyList.size()]);
        return ccl;
    }

    public final void ProceedOrder(View view){
        if(orderTotal>0f){
            if(orderTotal<=maximumOrder) {
                StringBuilder sbOrderAmountList = new StringBuilder();
                for (EditText et : etAmountList) {
                    if (et!=null && !TextUtils.isEmpty(et.getText().toString())) {
                        sbOrderAmountList.append(et.getText().toString()).append(",");
                    } else {
                        sbOrderAmountList.append("0").append(",");
                    }

                }
                String jsonDetailCurrencyList = gson.toJson(detailCurrencyList);

                editor.putString("detail_currency_list", jsonDetailCurrencyList);
                editor.putString("order_amount_list", sbOrderAmountList.toString());
                editor.putFloat("order_total", orderTotal);
                editor.commit();

                Intent intent = new Intent(DetailCurrencyActivity.this, ChooseShippingActivity.class);
                startActivity(intent);
            }else{
                BuildSimpleDialog(DetailCurrencyActivity.this.getString(R.string.error_title_exceed_maximum), DetailCurrencyActivity.this.getString(R.string.error_message_exceed_maximum)+"("+numberFormat.format(maximumOrder)+").", "Dismiss");
            }
        }else{
            BuildSimpleDialog(DetailCurrencyActivity.this.getString(R.string.error_title_null_order), DetailCurrencyActivity.this.getString(R.string.error_message_null_order), "Ok");
        }
    }

    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public class CollapsibleCurrencyAdapter extends ExpandableRecyclerViewAdapter<CollapsibleCurrencyAdapter.CollapsibleCurrencyViewHolder, CollapsibleCurrencyAdapter.ContentDetailCurrencyViewHolder> {

        List<CollapsibleCurrency> adapterCollapsibleCurrency;

        public CollapsibleCurrencyAdapter(List<? extends ExpandableGroup> groups) {
            super(groups);
            this.adapterCollapsibleCurrency=((List<CollapsibleCurrency>)groups);
        }

        public class CollapsibleCurrencyViewHolder extends GroupViewHolder {
            public TextView textName, textCode, textPrice;

            public CollapsibleCurrencyViewHolder(View itemView) {
                super(itemView);
                textName = itemView.findViewById(R.id.textName);
                textCode = itemView.findViewById(R.id.textCode);
                textPrice = itemView.findViewById(R.id.textPrice);
                textBuy = itemView.findViewById(R.id.textPriceBuy);
            }
            public void SetViewData(CurrencyList.Currency currency) {
                textCode.setText(currency.code);
                textName.setText(currency.name);
                textPrice.setText(currency.range);
                textBuy.setText(currency.buy_range);
            }
        }

        public class ContentDetailCurrencyViewHolder extends ChildViewHolder {
            public TextView tvDCValue, tvDCPrice;
            public EditText etDCAmount;

            public ContentDetailCurrencyViewHolder(View itemView) {
                super(itemView);
                tvDCValue = itemView.findViewById(R.id.tvDCValue);
                tvDCPrice = itemView.findViewById(R.id.tvDCPrice);
                etDCAmount = itemView.findViewById(R.id.etDCAmount);
                //etDCAmount.setText("0");
            }
            public void SetViewData(final CollapsibleCurrency.ContentDetailCurrency detailCurrency, String codeCurrency) {
                tvDCValue.setText(codeCurrency + Integer.toString(detailCurrency.nominal));
                tvDCPrice.setText(numberFormat.format(detailCurrency.price));
                etDCAmount.setText("");
                etDCAmount.addTextChangedListener(new TextWatcher() {
                    public void afterTextChanged(Editable s) {
                        if (!TextUtils.isEmpty(etDCAmount.getText().toString()) && Integer.parseInt(etDCAmount.getText().toString()) > detailCurrency.stock) {
                            etDCAmount.setText("");
                            BuildSimpleDialog(DetailCurrencyActivity.this.getString(R.string.error_title_insufficient_stock_2), DetailCurrencyActivity.this.getString(R.string.error_message_insufficient_stock_2), "Dismiss");
                        }
                        CalculateTotalOrder();
                    }
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
                    public void onTextChanged(CharSequence s, int start, int before, int count) {}
                });
                for(int i=0; i<detailCurrencyList.size(); i++){
                    if(detailCurrencyList.get(i).id==detailCurrency.id){
                        etAmountList.set(i,etDCAmount);
                    }
                }
            }
        }

        @Override
        public CollapsibleCurrencyViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_rates, parent, false);
            return new CollapsibleCurrencyViewHolder(view);
        }

        @Override
        public ContentDetailCurrencyViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_detailcurrency, parent, false);
            return new ContentDetailCurrencyViewHolder(view);
        }

        @Override
        public void onBindChildViewHolder(ContentDetailCurrencyViewHolder holder, int flatPosition, ExpandableGroup group,
                                          int childIndex) {
            final CollapsibleCurrency.ContentDetailCurrency detailCurrency = ((CollapsibleCurrency) group).getItems().get(childIndex);
            String codeCurrency=((CollapsibleCurrency) group).currency.code;
            holder.SetViewData(detailCurrency, codeCurrency);
        }

        @Override
        public void onBindGroupViewHolder(CollapsibleCurrencyViewHolder holder, int flatPosition,
                                          ExpandableGroup group) {
            holder.SetViewData(((CollapsibleCurrency) group).currency);
        }
    }
}
