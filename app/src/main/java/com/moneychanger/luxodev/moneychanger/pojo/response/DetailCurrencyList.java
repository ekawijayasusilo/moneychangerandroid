package com.moneychanger.luxodev.moneychanger.pojo.response;

import com.google.gson.annotations.SerializedName;
import com.moneychanger.luxodev.moneychanger.CollapsibleCurrency;

import java.util.ArrayList;
import java.util.List;

public class DetailCurrencyList {
    @SerializedName("maximumOrder")
    public Float maximumOrder;
    @SerializedName("currencyList")
    public List<CurrencyList.Currency> currencyList = new ArrayList();
    @SerializedName("detailCurrencyList")
    public List<DetailCurrency> detailCurrencyList = new ArrayList();

    public static class DetailCurrency {

        @SerializedName("id")
        public Integer id;
        @SerializedName("detail_currency_id")
        public Integer detailCurrencyId;
        @SerializedName("currency_id")
        public Integer currencyId;
        @SerializedName("nominal")
        public Integer nominal;
        @SerializedName("stock")
        public Integer stock;
        @SerializedName("price")
        public Float price;
        @SerializedName("codeCur")
        public String codeCur;

        public DetailCurrency(DetailCurrency item){
            this.id=item.id;
            this.detailCurrencyId=item.detailCurrencyId;
            this.currencyId=item.detailCurrencyId;
            this.nominal=item.nominal;
            this.stock=item.stock;
            this.price=item.price;
            this.codeCur=item.codeCur;
        }

    }
}
