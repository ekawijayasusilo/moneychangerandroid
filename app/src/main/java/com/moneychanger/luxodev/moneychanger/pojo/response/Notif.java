package com.moneychanger.luxodev.moneychanger.pojo.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Notif {
    @SerializedName("notifList")
    public List<Notif.Notify> notifList = new ArrayList();

    @SerializedName("order")
    public List<OrderList.Order> listHistoryOrder = new ArrayList();

    public class Notify {

        @SerializedName("id")
        public Integer id;
        @SerializedName("notification")
        public String notif;
        @SerializedName("date")
        public String date;
        @SerializedName("created_at")
        public String createdAt;
        @SerializedName("updated_at")
        public String updatedAt;

    }
}
