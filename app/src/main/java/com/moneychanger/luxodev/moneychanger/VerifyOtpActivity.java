package com.moneychanger.luxodev.moneychanger;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.moneychanger.luxodev.moneychanger.database.DatabaseHelper;
import com.moneychanger.luxodev.moneychanger.pojo.request.RegisterCustomer;
import com.moneychanger.luxodev.moneychanger.pojo.request.RegisterPhoneOTP;
import com.moneychanger.luxodev.moneychanger.pojo.response.OTPCode;
import com.moneychanger.luxodev.moneychanger.pojo.response.RegisteredCustomer;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RuntimePermissions
public class VerifyOtpActivity extends AppCompatActivity {

    String phoneNumber;
    String otpCode;
    String imeiNumber;
    TextView tvPhone;
    PinEntryEditText pinEntry;
    Boolean allowResend;
    Button btnResend;

    AlertDialog.Builder alertDialogBuilder;
    APIInterface apiInterface;
    SharedPreferences sharedPref;
    ProgressBar spinner;
    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);

        final VerifyOtpActivity verifyOtpActivity=this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        sharedPref=getSharedPreferences(VerifyOtpActivity.this.getString(R.string.app_name), Context.MODE_PRIVATE);
        alertDialogBuilder=new AlertDialog.Builder(VerifyOtpActivity.this);
        db = new DatabaseHelper(this);

        spinner = findViewById(R.id.progressBarVerifyOTP);
        spinner.setVisibility(View.GONE);

        allowResend=true;
        phoneNumber=sharedPref.getString("phone",null);
        otpCode=sharedPref.getString("otp_code",null);

        tvPhone= findViewById(R.id.tvPhone);
        tvPhone.setText(phoneNumber);
        btnResend=findViewById(R.id.btnResend);

        pinEntry = findViewById(R.id.pinOTP);
        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {
                    if (str.toString().equals(otpCode)) {
                        VerifyOtpActivityPermissionsDispatcher.GetImeiWithPermissionCheck(verifyOtpActivity);
                    } else {
                        pinEntry.setText(null);
                    }
                }
            });
        }

        //if(phoneNumber==null) ToRegister();
        //if(otpCode==null) requestNewOTP(phoneNumber,false);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void RegisterCustomer(){
        spinner.setVisibility(View.VISIBLE);
        RegisterCustomer regis;
        if(sharedPref.getString("type",null).equals("1")){
            String cardId=sharedPref.getString("card_id",null);
            regis=new RegisterCustomer("",imeiNumber,cardId,phoneNumber,"","","","");

        }
        else{
            String name=sharedPref.getString("name",null);
            String cardId=sharedPref.getString("card_id",null);
            String address=sharedPref.getString("address",null);
            String address2=sharedPref.getString("address2",null);
            String email=sharedPref.getString("email",null);
            String cardImage=db.getKTPImage(sharedPref.getLong("card_image_id",0));//BAGAIMANA JIKA SHAREDPREFNYA TIDAK KETEMU??
            regis=new RegisterCustomer(name,imeiNumber,cardId,phoneNumber,cardImage,address,address2,email);

        }

        Call<RegisteredCustomer> registeredCustomerCall = apiInterface.RegisterCustomerData(regis);
        registeredCustomerCall.enqueue(new Callback<RegisteredCustomer>() {
            @Override
            public void onResponse(Call<RegisteredCustomer> call, Response<RegisteredCustomer> response) {
                spinner.setVisibility(View.GONE);
                SharedPreferences.Editor editor = sharedPref.edit();
                //Toast.makeText(VerifyOtpActivity.this,Integer.toString(response.code()),Toast.LENGTH_LONG).show();
                if (response.code() == 200) {
                    RegisteredCustomer apiRes = response.body();

                    editor.putString("id",Integer.toString(apiRes.customer.id));
                    editor.putString("name",apiRes.customer.name);
                    editor.putString("email",apiRes.customer.email);
                    editor.putString("addrerss",apiRes.customer.address);
                    long regImageId=sharedPref.getLong("card_image_id",0);
                    if(regImageId==0){
                        regImageId=db.insertKTPImage(apiRes.customer.cardImage);
                    }
                    editor.putLong("card_image_id", regImageId);
                    editor.remove("otp_code");
                    editor.apply();

                    Intent intentWaitApproval = new Intent(VerifyOtpActivity.this,WaitApprovalActivity.class);
                    intentWaitApproval.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intentWaitApproval);
                }else if (response.code() == 460 || response.code() == 462){
                    editor.remove("name");
                    editor.remove("password");
                    editor.remove("phone");
                    editor.remove("card_id");
                    editor.remove("card_image_id");
                    editor.remove("address");
                    editor.remove("otp_code");
                    editor.commit();
                    if(response.code() == 460) BuildToRegisterDialog(VerifyOtpActivity.this.getString(R.string.error_title_different_phone_number), VerifyOtpActivity.this.getString(R.string.error_message_different_phone_number), "Dismiss");
                    else BuildToRegisterDialog(VerifyOtpActivity.this.getString(R.string.error_title_suspended_account), VerifyOtpActivity.this.getString(R.string.error_message_suspended_account), "Dismiss");
                }else if (response.code() == 470){
                    BuildSimpleDialog(VerifyOtpActivity.this.getString(R.string.error_title_used_phone_number), VerifyOtpActivity.this.getString(R.string.error_message_used_phone_number), "Dismiss");
                }else if(response.code()==480){
                    BuildSimpleDialog(VerifyOtpActivity.this.getString(R.string.error_title_unauthorized), VerifyOtpActivity.this.getString(R.string.error_message_unauthorized), "Dismiss");
                }else{
                    BuildSimpleDialog(VerifyOtpActivity.this.getString(R.string.error_title_server_error), VerifyOtpActivity.this.getString(R.string.error_message_server_error), "Dismiss");
                }
            }

            @Override
            public void onFailure(Call<RegisteredCustomer> call, Throwable t) {
                call.cancel();
                spinner.setVisibility(View.GONE);
                BuildSimpleDialog(VerifyOtpActivity.this.getString(R.string.error_title_internet), VerifyOtpActivity.this.getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }

    public void BuildToRegisterDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ToRegister();
                    }
                });
        alertDialog.show();
    }

    @NeedsPermission(Manifest.permission.READ_PHONE_STATE)
    public void GetImei(){
        TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        @SuppressLint("MissingPermission") String possibleImei = telephonyManager.getDeviceId();
        if (possibleImei != null && !possibleImei.isEmpty()) {
            imeiNumber=possibleImei;
        } else {
            imeiNumber=android.os.Build.SERIAL;
        }
        RegisterCustomer();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        VerifyOtpActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(Manifest.permission.READ_PHONE_STATE)
    void showRationaleForReadState(final PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_read_state_rationale)
                .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.READ_PHONE_STATE)
    void showDeniedForReadState() {
        Toast.makeText(this, R.string.permission_read_state_registration_denied, Toast.LENGTH_SHORT).show();
    }

    public void ToRegister(){
        Intent intentRegister = new Intent(VerifyOtpActivity.this,RegisterActivity.class);
        intentRegister.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intentRegister);
    }

    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void requestNewOTP(final String phone, final boolean isResend){
        spinner.setVisibility(View.VISIBLE);
        Call<OTPCode> phoneOTPCall = apiInterface.RegisterPhoneOTP(new RegisterPhoneOTP(phone));
        phoneOTPCall.enqueue(new Callback<OTPCode>() {
            @Override
            public void onResponse(Call<OTPCode> call, Response<OTPCode> response) {
                spinner.setVisibility(View.GONE);
                if (response.code() == 200) {
                    OTPCode apiRes = response.body();
                    otpCode=apiRes.otpCode;
                    phoneNumber=phone;
                    tvPhone.setText(phoneNumber);

                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("phone",phoneNumber);
                    editor.putString("otp_code",otpCode);
                    editor.apply();
                }else if(response.code()==480){
                    BuildSimpleDialog(VerifyOtpActivity.this.getString(R.string.error_title_unauthorized), VerifyOtpActivity.this.getString(R.string.error_message_unauthorized), "Dismiss");
                }else{
                    if(isResend) {
                        BuildSimpleDialog(VerifyOtpActivity.this.getString(R.string.error_title_invalid_phone), VerifyOtpActivity.this.getString(R.string.error_message_invalid_phone), "Dismiss");
                    }else{
                        ToRegister();
                    }
                }
            }

            @Override
            public void onFailure(Call<OTPCode> call, Throwable t) {
                call.cancel();
                spinner.setVisibility(View.GONE);

                BuildSimpleDialog(VerifyOtpActivity.this.getString(R.string.error_title_internet), VerifyOtpActivity.this.getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }

    public void ResendOTP(View view) {
        if(allowResend) {
            requestNewOTP(phoneNumber, true);
            allowResend=false;
            new CountDownTimer(60000, 1000) {
                public void onTick(long millisUntilFinished) {
                    btnResend.setText("Resend(" + Long.toString(millisUntilFinished/1000) +")");
                }
                public void onFinish() {
                    allowResend=true;
                    btnResend.setText("Resend");
                }

            }.start();
        }
    }
}
