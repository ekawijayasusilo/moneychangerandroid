package com.moneychanger.luxodev.moneychanger.pojo.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eka wijaya susilo on 3/28/2018.
 */

public class RegisterPhoneOTP {
    @SerializedName("phone")
    public String phone;

    public RegisterPhoneOTP(String phone){
        this.phone=phone;
    }
}
