package com.moneychanger.luxodev.moneychanger;

import android.os.Parcel;
import android.os.Parcelable;

import com.moneychanger.luxodev.moneychanger.pojo.response.CurrencyList;
import com.moneychanger.luxodev.moneychanger.pojo.response.DetailCurrencyList;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;


public class CollapsibleCurrency extends ExpandableGroup<CollapsibleCurrency.ContentDetailCurrency> {

    CurrencyList.Currency currency;

    public CollapsibleCurrency(){super(null, null);}

    public CollapsibleCurrency(String title, List<ContentDetailCurrency> items, CurrencyList.Currency currency) {
        super(title, items);
        this.currency=currency;
    }

    public class ContentDetailCurrency implements Parcelable {

        public Integer id;
        public Integer detailCurrencyId;
        public Integer currencyId;
        public Integer nominal;
        public Integer stock;
        public Float price;

        public ContentDetailCurrency(Integer id, Integer detailCurrencyId, Integer currencyId, Integer nominal, Integer stock, Float price){
            this.id=id;
            this.detailCurrencyId=detailCurrencyId;
            this.currencyId=currencyId;
            this.nominal=nominal;
            this.stock=stock;
            this.price=price;
        }

        public ContentDetailCurrency(DetailCurrencyList.DetailCurrency detailCurrency){
            this.id=detailCurrency.id;
            this.detailCurrencyId=detailCurrency.detailCurrencyId;
            this.currencyId=detailCurrency.currencyId;
            this.nominal=detailCurrency.nominal;
            this.stock=detailCurrency.stock;
            this.price=detailCurrency.price;
        }

        public ContentDetailCurrency(Parcel in) {
            this.id = in.readInt();
            this.detailCurrencyId = in.readInt();
            this.currencyId = in.readInt();
            this.nominal = in.readInt();
            this.stock = in.readInt();
            this.price = in.readFloat();
        }

        public final Creator<ContentDetailCurrency> CREATOR = new Creator<ContentDetailCurrency>() {
            @Override
            public ContentDetailCurrency createFromParcel(Parcel in) {
                return new ContentDetailCurrency(in);
            }

            @Override
            public ContentDetailCurrency[] newArray(int size) {
                return new ContentDetailCurrency[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.id);
            parcel.writeInt(this.detailCurrencyId);
            parcel.writeInt(this.currencyId);
            parcel.writeInt(this.nominal);
            parcel.writeInt(this.stock);
            parcel.writeFloat(this.price);
        }


    }
}
