package com.moneychanger.luxodev.moneychanger;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;

import com.moneychanger.luxodev.moneychanger.pojo.response.OrderList;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HistoryFragment extends Fragment {

    TabHost tabHost;
    Integer currentTabCode;

    APIInterface apiInterface;
    List<OrderList.Order> orderListOngoing;
    List<OrderList.Order> orderListHistory;

    ListView listViewOngoing;
    ListView listViewHistory;
    CustomAdapterOrder customAdapterOngoing;
    CustomAdapterOrder customAdapterHistory;
    SwipeRefreshLayout swipeRefresh,swipeGoingRefresh;
    TextView tvSelectedTab;

    SharedPreferences sharedPref;
    AlertDialog.Builder alertDialogBuilder;

    CardView cardOngoing;
    CardView cardHistory;
    ProgressBar loader;

    public static HistoryFragment newInstance() {
        return new HistoryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        alertDialogBuilder=new AlertDialog.Builder(getActivity());
        sharedPref=getActivity().getSharedPreferences(getActivity().getString(R.string.app_name), Context.MODE_PRIVATE);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        orderListOngoing=new ArrayList<>();
        orderListHistory=new ArrayList<>();
        currentTabCode=0;


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        tabHost=getView().findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec spec = tabHost.newTabSpec("ongoing");
        spec.setContent(R.id.tabOngoing);
        spec.setIndicator("Ongoing");
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec("history");
        spec.setContent(R.id.tabHistory);
        spec.setIndicator("History");
        tabHost.addTab(spec);

        for(int i=0;i<tabHost.getTabWidget().getChildCount();i++) {
            TextView tv = tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(getActivity().getResources().getColor(R.color.mcWhite));
        }
        swipeRefresh=getView().findViewById(R.id.swipeRefreshHistory);
        swipeGoingRefresh=getView().findViewById(R.id.swipeRefreshGoingHistory);

        cardOngoing=getView().findViewById(R.id.cardOngoing);
        cardHistory=getView().findViewById(R.id.cardHistory);
        cardOngoing.setVisibility(View.GONE);
        cardHistory.setVisibility(View.GONE);


        loader=getView().findViewById(R.id.progressBarHistory);
        loader.setVisibility(View.GONE);
        requestOrderList(0,true);
        requestOrderList(1,true);
        tvSelectedTab = tabHost.getTabWidget().getChildAt(currentTabCode).findViewById(android.R.id.title);
        tvSelectedTab.setTextColor(getActivity().getResources().getColor(R.color.mcSecondary));

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener(){
            @Override
            public void onTabChanged(String tabId) {
                if("ongoing".equals(tabId)) currentTabCode=0;
                if("history".equals(tabId)) currentTabCode=1;
                requestOrderList(currentTabCode,true);
                tvSelectedTab.setTextColor(getActivity().getResources().getColor(R.color.mcWhite));
                tvSelectedTab = tabHost.getTabWidget().getChildAt(currentTabCode).findViewById(android.R.id.title);
                tvSelectedTab.setTextColor(getActivity().getResources().getColor(R.color.mcSecondary));
            }
        });

        listViewOngoing=getView().findViewById(R.id.lvOngoing);
        customAdapterOngoing=new CustomAdapterOrder(0);
        listViewOngoing.setAdapter(customAdapterOngoing);

        listViewHistory=getView().findViewById(R.id.lvHistory);
        customAdapterHistory=new CustomAdapterOrder(1);
        listViewHistory.setAdapter(customAdapterHistory);

        swipeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        requestOrderList(currentTabCode,false);
                    }
                }
        );
        swipeGoingRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        requestOrderList(currentTabCode,false);
                    }
                }
        );
    }

    public void requestOrderList(final Integer requestCode, final boolean isRefresh){
        cardOngoing.setVisibility(View.GONE);
        cardHistory.setVisibility(View.GONE);
        if(isRefresh){ loader.setVisibility(View.VISIBLE);}
            Call<OrderList> currencyCall = apiInterface.GetOrderList(Integer.parseInt(sharedPref.getString("id",null)), requestCode);
        currencyCall.enqueue(new Callback<OrderList>() {
            @Override
            public void onResponse(Call<OrderList> call, Response<OrderList> response) {
                swipeRefresh.setRefreshing(false);
                swipeGoingRefresh.setRefreshing(false);

                if(response.code()==200) {
                    OrderList apiRes = response.body();
                    if (requestCode == 0){
                        orderListOngoing.clear();
                        orderListOngoing = apiRes.listHistoryOrder;
                        customAdapterOngoing.notifyDataSetChanged();
                        if(orderListOngoing.size()==0) cardOngoing.setVisibility(View.VISIBLE);
                        else cardOngoing.setVisibility(View.GONE);

                    }else if(response.code()==480){
                        BuildSimpleDialog(getActivity().getString(R.string.error_title_unauthorized), getActivity().getString(R.string.error_message_unauthorized), "Dismiss");
                    }else{
                        orderListHistory.clear();
                        orderListHistory = apiRes.listHistoryOrder;
                        customAdapterHistory.notifyDataSetChanged();
                        if(orderListHistory.size()==0) cardHistory.setVisibility(View.VISIBLE);
                        else cardHistory.setVisibility(View.GONE);
                    }
                     loader.setVisibility(View.GONE);

                }else{
                    BuildSimpleDialog(getActivity().getString(R.string.error_title_server_error), getActivity().getString(R.string.error_message_server_error), "Dismiss");
                    loader.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<OrderList> call, Throwable t) {
                call.cancel();
                if(isRefresh)swipeRefresh.setRefreshing(false);
                BuildSimpleDialog(getActivity().getString(R.string.error_title_internet), getActivity().getString(R.string.error_message_internet), "Dismiss");
            }
        });
    }

    public void BuildSimpleDialog(String dialogTitle, String dialogMessage, String buttonText){
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setTitle(dialogTitle);
        alertDialog.setMessage(dialogMessage);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, buttonText,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    class CustomAdapterOrder extends BaseAdapter {

        public Integer listViewCode;

        public CustomAdapterOrder(Integer listViewCode){
            this.listViewCode=listViewCode;
        }

//        public String DetermineStatusString(Integer statusCode){
//            switch(statusCode) {
//                case 1 :
//                    return "Pending Payment";
//                case 2 :
//                    return "On Process";
//                case 3 :
//                    return "On Shipping";
//                case 4 :
//                    return "Order Delivered";
//                default :
//                    return "Unknown";
//            }
//        }

        @Override
        public int getCount() {
            return listViewCode==0 ? orderListOngoing.size() : orderListHistory.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            view=getLayoutInflater().inflate(R.layout.listview_history,null);
            TextView textOrderNumber=view.findViewById(R.id.textOrderNumber);
            TextView textOrderStatus=view.findViewById(R.id.textOrderStatus);
            TextView textOrderDate=view.findViewById(R.id.textOrderDate);
            TextView textOrderTotalPay=view.findViewById(R.id.textOrderTotalPay);

            Locale localeID = new Locale("in", "ID");
            NumberFormat numberFormat = NumberFormat.getCurrencyInstance(localeID);

            final OrderList.Order currentOrder = listViewCode==0 ? orderListOngoing.get(i) : orderListHistory.get(i);

            textOrderNumber.setText(currentOrder.orderNumber);
            textOrderStatus.setText(currentOrder.statusName);
            textOrderDate.setText(currentOrder.date);
            textOrderTotalPay.setText(numberFormat.format(currentOrder.total));

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentDetailHistory = new Intent(getActivity(),DetailHistoryActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("current_order_id", currentOrder.id);
                    bundle.putBoolean("after_confirm_order",false);
                    bundle.putString("current_order_number",currentOrder.orderNumber);
                    intentDetailHistory.putExtras(bundle);
                    startActivity(intentDetailHistory);
                }
            });

            return view;
        }

    }
}
