package com.moneychanger.luxodev.moneychanger.pojo.response;

import com.google.gson.annotations.SerializedName;

public class CreatedOrder {
    @SerializedName("order")
    public CreatedOrderData order;

    public class CreatedOrderData {

        @SerializedName("id")
        public Integer id;
        @SerializedName("order_number")
        public String orderNumber;
//        @SerializedName("customer_id")
//        public Integer name;
//        @SerializedName("total")
//        public Float total;
//        @SerializedName("status")
//        public Integer status;
//        @SerializedName("delivery")
//        public Integer delivery;
//        @SerializedName("created_at")
//        public String createdAt;
//        @SerializedName("updated_at")
//        public String updatedAt;

    }
}
